/*WAP to the array elements in reverse order
Take array size and array elements from the user*/

#include<stdio.h>
void main(){

	int size;
	printf("enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("Array elements in reverse order are :\n");
	for(int i=size-1; i>=0; i--){
		printf("%d\n",arr[i]);
	}
}
