/*WAP to add two different arrays of the same size
Take array size and array elements from the user*/

#include<stdio.h>
void main(){

	int size;
	printf("enter array size :\n");
	scanf("%d",&size);

	int arr1[size];
	int arr2[size];

	printf("enter elements of first array:\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr1[i]);
	}

	printf("enter elements of second array:\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr2[i]);
	}

	printf("addition of arrays is :\n");
	
	for(int i=0; i<size; i++){
		printf("%d\n",arr1[i]+arr2[i]);
	}
}
