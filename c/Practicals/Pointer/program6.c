/*WAP to swap values of two numbers using a pointer.
(Hint: Use de-referencing of pointers)*/

#include<stdio.h>
void main(){
	int x;
	int y;

	printf("enter value of x and y :\n");
	scanf("%d %d",&x,&y);

	int *ptr1=&x;
	int *ptr2=&y;

	printf("before swapping:\n");

	printf("x is : %d\n",*ptr1);
	printf("y is : %d\n",*ptr2);

	ptr1=&y;
	ptr2=&x;

	printf("after swapping:\n");
	printf("x is : %d\n",*ptr1);
	printf("y is : %d\n",*ptr2);



}
