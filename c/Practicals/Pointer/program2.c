/*WAP to calculate the count of even and odd elements
Take array size and array elements from the user*/

#include<stdio.h>
void main(){
	int size;
	int count1=0;
	int count2=0;

	printf("enter array size\n");
	scanf("%d",&size);

	int iarr[size];

	printf("enter array elements\n");

	for(int i=0; i<size; i++){

		scanf("%d",&iarr[i]);
	}
	for(int i=0; i<size; i++){

		if(iarr[i]%2==0){
			count1++;
		}else{
			count2++;
		}
	}

	printf("count of even elements is : %d\n",count1);
	printf("count of odd elements is : %d\n",count2);
}
