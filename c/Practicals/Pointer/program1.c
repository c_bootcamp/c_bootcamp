/*WAP to find the given element from the array
Take array size and array elements from the user*/

#include<stdio.h>
void main(){
	int size;
	int num;
	
	printf("enter array size :\n");
	scanf("%d",&size);

	int iarr[size];

	printf("enter array elements :\n");

	for(int i=0; i<size; i++){

		scanf("%d",&iarr[i]);
	}

	int flag = 0;

	printf("enter the element to be search\n");
	scanf("%d",&num);

	for(int i=0; i<size; i++){
		if(num == iarr[i]){
			flag=1;
		}
	}

	if(flag==1){
		printf("%d is present\n",num);
	}else{
		printf("%d is not present\n",num);
	}
}
