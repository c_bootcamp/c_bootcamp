//DRAW PROPER DIAGRAM IN BOOK

#include<stdio.h>
void main(){

	int x=10;

	int *ptr=&x;		
	char *cptr=&x;		//(WARNING)

	printf("%d\n",*ptr);	//10
	printf("%d\n",cptr);	//INCOMPLETE ADDRESS OF X AS IT'S %d NOT %p (WARNING)

}
