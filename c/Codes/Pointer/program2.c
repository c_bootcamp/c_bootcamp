#include<stdio.h>
void main(){
	 
	int arr[5]={1,2,3,4,5};

	printf("%d\n",arr[0]);		//1
	printf("%d\n",arr[1]);		//2

	printf("%p\n",&arr[0]);		//ox100
	printf("%p\n",&arr[1]);		//ox104
	printf("\n");

	printf("%p\n",arr);		//ox 100
	printf("%p\n",&arr);		//ox 100

	int *iptr=arr;			
	int *iptr1=&arr;		//warning

	printf("%p\n",iptr);		//0x 100
	printf("%p\n",iptr1);		//ox 100
//	printf("%d",&arr++);		//error...arr++ la pn error yetoy...lvalue required mhanty....arr=arr+1..pn arr la box ch nahia
	int *ptr2= arr +1 ;
	int *ptr3= &arr +1 ;

	printf("%p\n",ptr2);		//0x 100
	printf("%p\n",ptr3);		//0x 100
	printf("%d\n",*ptr2);		//0x 100
	printf("%d\n",*ptr3);		//0x 100

//	ptr2= arr[0]++;			//warning
	ptr2= arr+ 1;			//no warning no error
	printf("%d\n",*ptr2);		//2
	printf("%p\n",ptr2);		//0x 104

	ptr2= arr++;			//error..lvalue required   ptr2=arr=arr+1;
}
