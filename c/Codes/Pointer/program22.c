//sum of array elements

#include<stdio.h>
void main(){

	int row;
	int col;
	int sum=0;

	printf("enter number of rows:\n");
	scanf("%d",&row);

	printf("enter number of colm:\n");
	scanf("%d",&col);

	int arr[row][col];

	printf("enter array elements:\n");
	for(int i=0; i<row; i++){
		for(int j=0; j<col; j++){
			scanf("%d",&arr[i][j]);
		}
	}

	printf("array elements are:\n");
	for(int i=0; i<row; i++){
		for(int j=0; j<col; j++){
			printf("%d\t",arr[i][j]);
			sum= sum+arr[i][j];
		}
		printf("\n");
	}
	printf("sum of array elements is :%d\n",sum);
}
