//POINTER OUTPUT ON DATA AND SPECIFIERS

#include<stdio.h>
void main(){
	
	int x=10;

	int *ptr1=&x;
	int *ptr2=x;			//..WARNING..pointer(int*) la address pahije pn tu detanna integer value(x) detey

	printf("%d\n",x);		//10
	printf("%p\n",x);		//...WARNING..//0xa ...(10) la hexadecimal ghetly
	printf("%p\n",&x);		//0x100
	printf("%p\n",ptr1);		//0x100
	printf("%p\n",ptr2);		//...WARNING...//0xa
					//0xa...10 (*ptr2=x=10)la hexadecimal mdhe 0xa lihita
	printf("%p\n",&ptr1);
	printf("%p\n",&ptr2);		
}
