//***Void Pointer

#include<stdio.h>
void main(){
	int x = 10;

	int *iptr=&x;
	void *vptr=&x;
	
	printf("%p\n",iptr);	//0x100
	printf("%p\n",vptr);	//0x100

	printf("%d\n",*iptr);	//10
//	printf("%d\n",*vptr);	//warning and Error -->invalid use of void expression-->void* la dereference krta yet nahi

	printf("%d\n",*((int*)vptr));	//10 -->Typecast krun dereference krta yet
}
