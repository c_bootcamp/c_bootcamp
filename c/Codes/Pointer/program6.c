//ASSIGNING ARRAY TO AN ARRAY

#include<stdio.h>
void main(){
	int arr1[3];
	arr1[0]=10;
	arr1[1]=20;
	arr1[2]=30;

	printf("Array-1 is :\n");
	for(int i=0; i<3; i++){
		printf("%d\n",arr1[i]);
	}
	
	int arr2[3];

	//AASIGNING ARRAY TO AN ARRAY
	//arr2=arr1;	--> error ...address madhech address taktoy...array_name address of its first element
	
	printf("Array-2 is :\n");
	for(int i=0; i<3; i++){
		arr2[i]=arr1[i];
		printf("%d\n",arr2[i]);
	}
	
}

