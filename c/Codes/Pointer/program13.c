//dereferencing

#include<stdio.h>
void main(){

	char ch= 'A';

	int *iptr=&ch;	//warning
	char *cptr=&ch;

	printf("%p\n",iptr);	//0x100
	printf("%p\n",cptr);	//0x100

	printf("%d\n",*iptr);	//<confusion>***garbage value***
	printf("%d\n",*cptr);	//65

	printf("%c\n",*iptr);	//A
	printf("%c\n",*cptr);	//A
}
