//Multi dimensional Array ==>first subscript sodun pudchya sglya subscript la value dyavi lagte

#include<stdio.h>
void main(){
	
	printf("array 1:\n");
	int arr1[3][3] ={1,2,3,4,5,6,7,8,9};
	for(int i=0; i<3;i++){
		for(int j=0; j<3; j++){
			printf("%d\t",arr1[i][j]);
		}
		printf("\n");
	}

	printf("array 2:\n");
	int arr2[][3] ={{1,2,3},{4,5,6},{7,8,9}};
	for(int i=0; i<3;i++){
		for(int j=0; j<3; j++){
			printf("%d\t",arr2[i][j]);
		}
		printf("\n");
	}

	printf("array 3:\n");
	int arr3[3][3] ={1,2,3,4,5};
	for(int i=0; i<3;i++){
		for(int j=0; j<3; j++){
			printf("%d\t",arr3[i][j]);
		}
		printf("\n");
	}

	printf("array 4:\n");
	int arr4[3][3] ={1,2,3,4,{5,6},{7,8,9}};
	for(int i=0; i<3;i++){
		for(int j=0; j<3; j++){
			printf("%d\t",arr4[i][j]);
		}
		printf("\n");
	}

	printf("array 5:\n");
	int arr5[2][3] ={1,2,3,4,5,6,7,8,9};
	for(int i=0; i<2;i++){
		for(int j=0; j<3; j++){
			printf("%d\t",arr5[i][j]);
		}
		printf("\n");
	}
	printf("array 6:\n");
	int arr6[3][3] ={{1,2,3,4},{5,6,7},{8,9}};
	for(int i=0; i<3;i++){
		for(int j=0; j<3; j++){
			printf("%d\t",arr6[i][j]);
		}
		printf("\n");
	}
//	printf("%d\n",arr6[0][3]); ==> 4 la ignore krtyo..aani tgithe 5 la jaga deto aani arr6[0][3] la 5 detoy
}

