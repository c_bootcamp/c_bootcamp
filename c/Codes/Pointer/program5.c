//array comparision

#include<stdio.h>
void main(){
	int size;
	printf("enter array size :\n");
	scanf("%d",&size);

	int arr1[size];
	int arr2[size];

	printf("enter array1 elements\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr1[i]);
	}
	printf("enter array2 elements\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr2[i]);
	}

	/*there is no proper o/p since both addresses will always be different..and not correct way of comparing arrays
	if(arr1==arr2){
		printf("both arrays are same\n");
	}else{
		printf("both arrays are not same\n");
	}
	 */  
	int flag=0;

	for(int i= 0; i<size; i++){
		if(arr1[i]==arr2[i]){
			flag=1;
		}else{
			flag=0;
			break;
		}
	}
	if(flag==1){
		printf("both arrays are same\n");
	}else{
		printf("both arrays are not-same\n");
	}
}
