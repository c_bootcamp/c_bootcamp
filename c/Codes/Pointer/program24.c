//2-D array

#include<stdio.h>
void main(){
	int arr[3][3]={10,20,30,40,50,60,70,80,90};

	printf("%p\n",arr);		//pointing towards first row...address of first row
	printf("%p\n",&arr);		//address of complete array

	printf("%p\n",arr[0]);		//addresses of rows
	printf("%p\n",arr[1]);		
	printf("%p\n",arr[2]);

	printf("%d\n",arr[0][0]);	 //data
	printf("%d\n",arr[1][0]);
	printf("%d\n",arr[2][0]);

	printf("%p\n",*&arr);	
	printf("%d\n",***&arr);	//data la gela               
	printf("%p\n",*arr)     //first row chya address la hota..dereference kel tr first element chya address la gela
	printf("%d\n",**arr);   //data la gela     
}
