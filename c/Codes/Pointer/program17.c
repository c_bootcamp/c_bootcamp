#include<stdio.h>
void main(){

	int arr[]={10,20,30,40,50};

	int *iptr = &(arr[0]);		//100

	printf("%p\n",iptr);		//100
	printf("%d\n",*iptr);		//10

	iptr++;				
	printf("%p\n",iptr);		//104

	*iptr=70;	//left la *iptr mhanje box aahe and right la *iptr asla tr ti  value/data asto

	for(int i=0; i<5; i++){
		printf("%d\n",arr[i]);	//10 70 30 40 50
	}

	printf("%p\n",iptr);		//104

	printf("%d\n",(*iptr++));	//70 -->post mhant ki tuz kam kr aadhi pn kam nahia kahi so te temp return krun increment krt
	printf("%p\n",iptr);		//108

	printf("%d\n",(*iptr++));	//30 --> x++ => x(*iptr) mhanje temp variable return krto (post) aani mg x(iptr) mdhe increment krto
	printf("%p\n",iptr);		//112

	printf("%d\n",(*iptr)++);	//40 --> post mhant ki tuz kam krr aadhi ..so to (*iptr) krel i.e. 40 aani temp mdhe return krel(40) then increment i.e. (40+1=41)
	printf("%d\n",*iptr);		//41
}
