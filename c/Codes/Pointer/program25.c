//2-D array

#include<stdio.h>
void main(){
	int arr[3][3]={10,20,30,40,50,60,70,80,90};

	printf("%d\n",arr[0][2]);	//data
	printf("%d\n",arr[1][0]);
	printf("%d\n",arr[2][1]);

	printf("%p\n",arr);		//address of 1st row in 2-D
	printf("%p\n",&arr);		//address of complete array

	printf("%p\n",arr[0]);		//address of 1 st element in 1 st row 
	printf("%d\n",*arr[0]);
	printf("%p\n",arr[1]);		//address of 1 st element in 2 nd row
	printf("%d\n",*arr[1]);
	printf("%p\n",arr[2]);		//address of 1 st element in 3 rd row
	printf("%d\n",*arr[2]);
}
