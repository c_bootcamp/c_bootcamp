//Substraction of pointers --> Meaningful in Arrays only -->returns long value

#include<stdio.h>
void main(){
	int arr[]={1,2,3,4,5};

	int *ptr1 = &(arr[0]);
	int *ptr2 = &(arr[3]);

	printf("%d\n",*ptr1);	//1
	printf("%d\n",*ptr2);	//4

	//substraction of pointers
	
	printf("%ld\n",ptr2-ptr1);	//3 --> (ptr2-ptr1)/sizeof(DTP)	
	printf("%ld\n",ptr1-ptr2);	//-3--> (ptr1-ptr2)/sizeof(DTP)
}
