//Null Pointer and Wild pointer

#include<stdio.h>
void main(){
	int x=10;
	
	int *ptr;
	int *ptr1 =NULL;
	int *ptr2=0;

	printf("%p\n",ptr);	//nil
	printf("%d\n",*ptr);	//segmentation fault

	printf("%p\n",ptr1);	//nil
	printf("%d\n",*ptr1);	//segmentation fault

	printf("%p\n",ptr2);	//nil
	printf("%d\n",*ptr2);	//segmentation fault
}
