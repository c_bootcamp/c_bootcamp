//***ARRAY***
//
#include<stdio.h>
void main(){
	int arr[]={10,20,30,40,50};
	
	printf("array elements are:\n");
	printf("%d\n",arr[0]);
	printf("%d\n",arr[1]);
	printf("%d\n",arr[2]);
	printf("%d\n",arr[3]);
	printf("%d\n",arr[4]);

	printf("addresses\n");

	printf("%p\n",&arr[0]);
	printf("%p\n",&arr[1]);
	printf("%p\n",&arr[2]);
	printf("%p\n",&arr[3]);
	printf("%p\n",&arr[4]);
	
	printf("\n");	

	int *ptr1=arr;
	printf("%p\n",ptr1);

	//int *ptr2=&arr; ---- warning ....eka pointer mdhe complete array cha address store krtoy..chukich aahe..ya stahi pointer to an array concept aahe !!
	
	int (*ptr2)[5]=&arr;	//pointer to an array...no warning
	printf("%p\n",ptr2);

	ptr1=arr+1;
	printf("%p\n",ptr1);//arr ha first element kde bghtoy..tyala increment kel 1 ne tr to next element la point krel 

	ptr2=&arr+1;	//&arr ha purn array kde bghtoy..tyala increment kel tr to purn array skip krun 1 ne pudhe jail
	printf("%p\n",ptr2);

	printf("dereference\n");

	printf("%d\n",*ptr1);		//20
	printf("%d\n",*ptr2);		//GV at that address
} 
