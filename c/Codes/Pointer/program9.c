//DEREFERENCING OF POINTER

#include<stdio.h>
void main(){
	int x=10;

	int *ptr1=&x;
	int *ptr2=x;		//warning

	printf("%p\n",ptr1);	//0x100	
	printf("%p\n",ptr2);	//0xa

	printf("%d\n",*ptr1);	//10
	printf("%d\n",*ptr2);	//segmentation fault(core dump)-->mhanun pointer la kdhich data na dyaycha..dereference kel ki to process space chya baher jato
}
