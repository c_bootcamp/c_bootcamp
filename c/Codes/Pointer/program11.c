/*OPERATIONS ON POINTER 	-->ptr + ptr == error
				-->ptr + int == chalt
			 	-->ptr - ptr == chalt ...meaningful in arrays
				-->ptr - int == chalt
				-->ptr + char == chalt
				-->ptr + float/double== error
				-->ptr * ptr == error
				-->ptr * int == error
				-->ptr / ptr == error
				-->ptr / int == error
				-->ptr % ptr == error
				-->ptr % int == error
				
*/

#include<stdio.h>
void main(){

	int x= 10;
	int y= 20;

	int *ptr1= &x;
	int *ptr2= &y;

	printf("%p\n",ptr1);		//0x100
	printf("%p\n",ptr2);		//0x104

	printf("%d\n",*ptr1);		//10
	printf("%d\n",*ptr2);		//20

	printf("%d\n",*ptr1 + *ptr2);	//10+20 = 30
//	printf("%d\n",ptr1 + ptr2);	//error : invalid operands to binary '+' -->pointer aani pointer chi addition hott nahi!!

	printf("%p\n",ptr1 +1);		//(pointer + integer)<-- chalt ...ethla change mi kuthe store nahi kelay  -->0x104
	printf("%d\n", *(ptr1 + 1));	//pointer aani integer chi addition chalte -->20

	printf("%p\n",ptr1);		//0x100
	printf("%d\n",*ptr1);		//10
	
//	printf("%d\n",ptr1 + 1.5f);	//error -->invalid operand to binary '+' (have int* and float)
	printf("%p\n",ptr1 + 'A');	//address
	printf("%d\n",*(ptr1 + 'A'));	//GV
}
