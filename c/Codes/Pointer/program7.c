/*POINTER......................[%d --> integer--.4 byte]
			       [%c --> character-->1 byte]
			       [%f --> float-->4 byte]
			       [%lf -->double-->8 byte]
			       [%ld -->long-->8 byte]
			       [%p --> void* i.e for pointer-->8 byte]
*/

#include<stdio.h>
void main(){

	int x= 10;

	printf("%p\n",&x);	//variable sathi pointer aani & same ch work krt..aani same address dakhvt..(proper o/p)
				//%p ha 8 byte vachto..aani hexadecimal return krt...address print krnya sathi use hot..!!
	int *ptr=&x;

	printf("%p\n",ptr);	//%p is used to for addresses..it can read 8 bytes and returns hexadecimal value..gives adresses in hexadecimal form..!!(proper o/p)

	printf("%d\n",&x);	//address ha 8 byte cha asto pn %d ha 4 ch byte vachu shakto (int format mdhe) mhanun to half ch address dakhvel..integer madhe..!!..(improper o/p) ---->WARNING

	printf("%ld\n",&x);   //%ld ha pn 8 byte vachto pn integer format mdhe (pn he actual peksha jast address dakhvt)...(improper o/p) ---->WARNING
}
