//Dangling pointer 

#include<stdio.h>
int a= 10;
int b;
int *ptr;

void gun(){
	printf("in gun\n");
}

void fun(){
	int x= 30;

	printf("%d\n",a);
	printf("%d\n",b);

	ptr=&x;

	printf("%p\n",ptr);
	printf("%p\n",&x);
	printf("%d\n",*ptr);		//30
}

void main(){

	int y=40;

	printf("%d\n",a);
	printf("%d\n",b);

	fun();

	printf("%d\n",*ptr);		//30

	gun();		//gun() chi stack geli aata... fun() chi pop zali

	printf("%d\n",*ptr);		//0
}
