#include<stdio.h>
void main(){
	int arr1[]={10,20,30,40};
	int arr2[]={50,60,70,80};

	int *ptr1=arr1;
	int *ptr2=&arr1;	//warning...pointer to an array ==>POINTER TO AN ARRAY

	printf("%p\n",ptr1);
	printf("%p\n",ptr2);

	ptr1++;
	ptr2++;

	printf("%d\n",*ptr1);	//20
	printf("%d\n",*ptr2);	//20

	ptr1=arr1+1;
	ptr2=&arr1+1;

	printf("%d\n",*ptr1);	//20
	printf("%d\n",*ptr2);	//50 ..complete array skip krt aani next location la jat

	printf("%p\n",ptr1);
	printf("%p\n",ptr2);	//arrays la continuous memory bhettey
	printf("%p\n",arr2);	//ptr2 aani arr2 same aahe

	int *ptr3=&arr1+1;
	printf("%d\n",*ptr3);	//50..siran ch GV yet
}
