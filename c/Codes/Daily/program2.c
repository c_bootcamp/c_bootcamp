//function --> call by address (2-D) ==== using 1-FOR LOOP only

#include<stdio.h>

void arrCall(int (*ptr)[],int size){
	for(int i=0; i<size; i++){
		printf("%d\n",(*(*ptr+i)));
	}
}

void main(){
	int arr[3][3] ={1,2,3,4,5,6,7,8,9};
	int size= sizeof(arr)/sizeof(int);

	arrCall(arr,size);
	printf("%d\n",*(*arr+0));
}
