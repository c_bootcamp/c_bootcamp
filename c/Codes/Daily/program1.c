//function --> call by address(1-D array)

#include<stdio.h>

void arrCall1(int *ptr, int size){

	for(int i=0; i<size; i++){
		printf("%d\n",*(ptr+i));
	}
}

void arrCall2(int ptr[], int size){

	for(int i=0; i<size; i++){
		printf("%d\n",*(ptr+i));
	}
}

void main(){

	int arr[] = {10,20,30,40,50};
	int size = sizeof(arr)/sizeof(int);
	
	arrCall1(arr,size);
	arrCall2(arr,size);
}

