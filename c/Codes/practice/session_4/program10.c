/*
	4	3	2	1
	C	B	A
	2	1
	A
*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	int ch1=64+rows;
	int num1=rows;

	for(int i=1; i<=rows; i++){
		int num2=num1;
		int ch2=ch1;
		for(int j=rows; j>=i; j--){
			if(i%2!=0){
				printf("%d\t",num2);
			}else{
				printf("%c\t",ch2);
			}
			num2--;
			ch2--;
		}
		num1--;
		ch1--;
		printf("\n");
	}
}
