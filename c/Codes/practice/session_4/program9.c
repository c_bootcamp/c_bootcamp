/*
D	C	B	A
C	B	A
B	A			
A
*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	int ch1=64+rows;

	for(int i=1; i<=rows; i++){
		int ch2=ch1;
		for(int j=rows; j>=i; j--){
			printf("%c\t",ch2);
			ch2--;
		}
		ch1--;
		printf("\n");
	}
}
