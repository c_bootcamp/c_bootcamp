/*
	a	b	c	d
	a	b	c
	a	b
	a
*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		int ch=97;
		for(int j=rows; j>=i; j--){
			printf("%c\t",ch);
			ch++;
		}
		printf("\n");
	}
}
