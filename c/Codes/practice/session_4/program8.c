/*
 d	d	d	d
 C	C	C
 b	b
 A
*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	int ch1=96+rows;
	int ch2=64+rows;

	for(int i=1; i<=rows; i++){
		for(int j=rows; j>=i; j--){
			if(i%2!=0){
				printf("%c\t",ch1);
			}else{
				printf("%c\t",ch2);
			}
		}
		ch1--;
		ch2--;
		printf("\n");
	}
}
