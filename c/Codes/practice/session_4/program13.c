//WAP to print the factorial of each number between a given range

#include<stdio.h>
void main(){

	int num1,num2;

	printf("enter start value:\n");
	scanf("%d",&num1);

	printf("enter end value:\n");
	scanf("%d",&num2);

	for(int i=num1; i<=num2; i++){
		int fact=1;
		for(int j=i;j>=1;j--){
			fact=fact*j;
		}
		printf("the Factorial of %d is : %d\n",i,fact);
	}
}
