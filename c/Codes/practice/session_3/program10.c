/* ---->>>> WRONG FOR OTHER NUMBER OF ROWS <<<<<----
 
10
I 	H
7	6	5
D	C	B	A

*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	int num=10;
	char ch=64+num;

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=i; j++){
			if(i%2 != 0){
				printf("%d\t",num);
			}else{
				printf("%c\t",ch);
			}
			num--;
			ch--;
		}
		printf("\n");
	}
}
