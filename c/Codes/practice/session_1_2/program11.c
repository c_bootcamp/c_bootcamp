/*
a
b c
d e f
g h i j
*/

#include<stdio.h>
void main(){
	char ch= 'a';
	int rows;

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=i; j++){
			printf("%c\t",ch);
			ch++;
		}
		printf("\n");
	}
}
