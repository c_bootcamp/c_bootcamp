/*
d	d	d	d
c	c	c	c
b	b	b	b
a	a	a	a
*/

#include<stdio.h>
void main(){
	int ch;
	int rows;

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	ch=96+rows;

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%c\t",ch);	
		}
		ch--;
		printf("\n");
	}

}
