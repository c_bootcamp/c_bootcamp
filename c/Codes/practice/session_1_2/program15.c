/*
3
6 9
12 15 18
*/

#include<stdio.h>
void main(){
	int num=1;
	int rows;

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows; i++){
		for(int j=1; j<=i; j++){
			printf("%d\t",rows*num);
			num++;
		}
		printf("\n"); 
	}
}
