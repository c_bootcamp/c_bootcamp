/*
a B c
d E f
g H i
*/

#include<stdio.h>
void main(){
	int ch1 =97;
	int ch2=65;
	int rows;

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=1; j<=rows; j++){
			if(j%2 != 0){
				printf("%c\t",ch1);
			}else{
				printf("%c\t",ch2);
			}
			ch1++;
			ch2++;
		}
		printf("\n");
	}
}
