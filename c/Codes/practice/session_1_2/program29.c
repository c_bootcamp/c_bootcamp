/*
A b C
d E f
G h I
*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows :\n");
	scanf("%d",&rows);

	int ch1=65,ch2=97;

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			if(i%2==0 && j%2 == 0||i%2!=0 && j%2!=0){
				printf("%c\t",ch1);
			}else{
				printf("%c\t",ch2);
			}
			ch1++;
			ch2++;
		}
		printf("\n");
	}
}
