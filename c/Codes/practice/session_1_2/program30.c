/*
1	3	5
5	7	9
9	11	13
*/

#include<stdio.h>
void main(){
	int rows;
	int num1=1;
	int num;

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		num=num1;
		for(int j=1 ;j<=rows; j++){
			printf("%d\t",num);
			num1=num;
			num=num+2;
		}
		printf("\n");
	}
}
