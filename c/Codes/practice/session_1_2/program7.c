/*
A A A 
B B B
C C C
*/

#include<stdio.h>
void main(){
	char ch;
	int rows;

	printf("enter any character:\n");
	scanf("%c",&ch);
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%c\t",ch);
		}
		printf("\n");
		ch++;
	}
}
