/*
1 2 3 4
a b c d 
5 6 7 8
e f g h
*/
#include<stdio.h>
void main(){
	int num,rows;
	char ch;

	printf("enter any number:\n");
	scanf("%d",&num);

	printf("enter any character:\n");
	scanf(" %c",&ch);

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		for(int j=1;j<=rows;j++){
			if(i%2 != 0){
				printf("%d\t",num);
				num++;
			}else{
				printf("%c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}
}
