/*square of odd numbers between given range*/

#include<stdio.h>
void main(){
	int num1,num2;

	printf("Enter start:\n");
	scanf("%d",&num1);

	printf("Enter end:\n");
	scanf("%d",&num2);

	if(num1<num2){
		for(int i= num1;i<=num2;i++){
			if(i%2 != 0){
				printf("%d\t",i*i);
			}
		}
	}else if(num1>num2){
		for(int i= num2;i<=num1;i++){
			if(i%2 != 0){
				printf("%d\t",i*i);
			}
		}
	}else{
		printf("Enter valid input");
	}
	printf("\n");
}
