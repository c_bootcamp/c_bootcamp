/*
4 
8	12
16	20	24
28	32	36	40
*/

#include<stdio.h>
void main(){
	int num=1;
	int rows;

	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1;i<=rows; i++){
		for(int j=1; j<=i; j++){
			printf("%d\t",rows*num);
			num++;
		}
		printf("\n"); 
	}
}
