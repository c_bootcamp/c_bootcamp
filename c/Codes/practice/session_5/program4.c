/*	ROWS=4				|	ROWS=3					
				1       |			1
			5	9	|		4	7
		13	17	21	|	10	13	16
	25	29	33	37	|
*/

#include<stdio.h>
void main(){
	int rows;
	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	int num= 1;
	
	for(int i=1; i<=rows; i++){
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			printf("%d\t",num);
			num=num+rows;
		}
		printf("\n");
	}
}
