/*
				d
			c	c
		b	b	b
	a	a	a	a
*/

#include<stdio.h>
void main(){
	int rows;
	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	int ch=96+rows;
	
	for(int i=1; i<=rows; i++){
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			printf("%c\t",ch);
		}
		ch--;
		printf("\n");
	}
}
