/*	***WRONG***
				1
			4	9
		64	125	216
	2401	4096	6561	10000
*/

#include<stdio.h>
void main(){

	int num=1;
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			int mul=1;
			int k=1;

			do{
				mul=mul*num;
				k++;
			}while(k<=i);

			printf("%d\t",mul);
			num++;
		}
		printf("\n");
	}
}
