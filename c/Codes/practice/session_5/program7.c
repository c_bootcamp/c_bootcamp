/*
					A
				b	a
			C	E	G
		d	c	b	a
	E	G	I	K	M
*/

#include<stdio.h>
void main(){
	int rows,ch3,ch4;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	int ch1=65;
	int ch2=97;

	for(int i=1; i<=rows; i++){
		ch3=ch1;
		ch4=ch2;
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			if(i%2!=0){
				printf("%c\t",ch3);
				ch3=ch3+2;
			}else{
				printf("%c\t",ch4);
				ch4--;
			}
		}
		ch1++;
		ch2++;
		printf("\n");
	}
}
