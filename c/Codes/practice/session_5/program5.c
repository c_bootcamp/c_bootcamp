/*
				D
			c	D
		B	c	D
	a	B	c	D
*/

#include<stdio.h>
void main(){
	int rows;
	printf("Enter number of rows:\n");
	scanf("%d",&rows);

	int ch1=96+rows;
	int ch2=64+rows;
	
	for(int i=1; i<=rows; i++){
		int count=0;
		for(int space=rows; space>i; space--){
			count++;
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			int num=j+count;
			if(num%2!=0){
				printf("%c\t",ch1);
			}else{
				printf("%c\t",ch2);
			}
			ch1++;
			ch2++;
		}
		ch1=ch1-i-1;
		ch2=ch2-i-1;
		printf("\n");
	}
}
