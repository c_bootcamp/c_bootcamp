/*
			1
		1	2
	1	2	3
1	2	3	4
*/

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		int num=1;
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}
