/*
				4
			3	6
		2	4	6
	1	2	3	4
*/  

#include<stdio.h>
void main(){
	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	int num=rows;

	for(int i=1; i<=rows;i++){
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			printf("%d\t",num*j);
		}
		num--;
		printf("\n");
	}
}
