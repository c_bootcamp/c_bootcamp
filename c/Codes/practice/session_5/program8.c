/*
			1
		A	b
	1	2	3
A	b	C	d
*/

#include<stdio.h>
void main(){

	int rows;
	printf("enter number of rows:\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		int num=1;
		int ch1=65;
		int ch2=97;
		for(int space=rows; space>i; space--){
			printf("\t");
		}
		for(int j=1; j<=i; j++){
			if(i%2!=0){
				printf("%d\t",num);
				num++;
			}else{
				if(j%2!=0){
					printf("%c\t",ch1);
				}else{
					printf("%c\t",ch2);
				}
				ch1++;
				ch2++;
			}
		}
		printf("\n");
	}
}
