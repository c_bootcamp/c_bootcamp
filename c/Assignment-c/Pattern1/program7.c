
/*
1   2     9     4
25  6     48    8
81  10    121   12
169 14    125   16
*/

#include<stdio.h>
void main(){
	int rows;
	int num;

	printf("enter number of rows\n");
	scanf("%d",&rows);

	printf("enter any number\n");
	scanf("%d",&num);

	 for(int i=1; i<=rows; i++){
		 for(int j=1; j<=rows ; j++){
			if(j%2 != 0){
				printf("%d\t ", num*num);
			}else{
				printf("%d\t ",num);
			}
			num++;
		 }
		 printf("\n");
	 }
}
