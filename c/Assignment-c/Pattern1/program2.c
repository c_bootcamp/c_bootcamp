/*
  1 2 3
  a b c
  1 2 3
  a b c
*/

#include<stdio.h>
void main(){

	int row;

	printf("Enter number of rows\n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){

		int num=1;
		char ch='a';

		for(int j=1; j<=row-1; j++){
			if(i%2!=0){
				printf("%d ",num);
				num++;
			}else{
				printf("%c ",ch);
				ch++;
			}
		}
	printf("\n");
	}
}
