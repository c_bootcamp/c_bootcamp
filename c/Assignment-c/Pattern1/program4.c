/* A B C
   D E F
   G H I */

#include<stdio.h>
void main() {

	int row;
	char ch='A';
	printf("Enter the number of rows\n");
	scanf("%d",&row);

	for(int i=1; i<=row;i++){
		for(int j=1; j<=3; j++){
			printf("%c ",ch);
			ch++;
		}
		printf("\n");
	}
}
