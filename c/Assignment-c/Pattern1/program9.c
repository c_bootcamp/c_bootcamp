/*
  2  5  10
  17 26 37
  50 65 82
*/

#include<stdio.h>
void main() {
	int rows;
	int num;

	printf("Enter number of rows\n");
	scanf("%d",&rows);

	printf("Enter any number\n");
	scanf("%d",&num);

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d ",num*num+1);
			num++;
		}
		printf("\n");
	}
}
