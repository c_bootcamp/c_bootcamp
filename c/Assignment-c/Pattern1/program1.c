/*
 1 2 3 4
 2 3 4 5
 3 4 5 6
 4 5 6 7
*/

#include<stdio.h>
void main(){
	int row;
	int num;

	printf("Enter number of rows\n");
	scanf("%d",&row);

	printf("Enter any number\n");
	scanf("%d",&num);

	for(int i=1; i<=row; i++){
		num=i;
		for(int j=1; j<=4; j++){
			printf("%d ",num);
			num++;
	}
	printf("\n");
	}
}
