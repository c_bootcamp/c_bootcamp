//9: to print sum of first 10 odd numbers

#include<stdio.h>
void main() {

     int num1,num2,count=0,sum=0;

     printf("Enter starting number:\n");
     scanf("%d",&num1);

     printf("Enter ending number:\n");
     scanf("%d",&num2);

     for(int i=num1 ; i<=num2 ; i++) {
          
           if(i%2==1 && count<10) {

                 printf("%d\n",i);
                 sum=sum+i;
                 count++;
           } 
    }
    printf("the sum of first 10 odd numbers between %d and %d is : %d \n",num1,num2,sum);
}

