//6 :WAP to print reverse from 100-1

#include<stdio.h>
void main(){

     int num1,num2;

     printf("enter the first number :\n");   
     scanf("%d",&num1);  

     printf("enter the second number :\n");   
     scanf("%d",&num2); 

     printf("the numbers in reverse order in given range are \n"); 

     for(int i = num2 ; i >= num1 ; i--) {

          printf("%d\n",i);
     }
}
