// 8 : to print table of 11 in reverse order

#include<stdio.h>
void main() {

     int num1;
     int tbl = 0;

     printf("Enter any number\n");
     scanf("%d",&num1);
      
     tbl = num1*10;

     printf("the table of %d is : \n",num1);
     
     for(int i = 1 ; i <= 10 ; i++) {
 
                printf("%d\n",tbl);
                tbl = tbl - num1;
     }
}
