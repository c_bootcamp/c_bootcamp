/*
0 3 8
15 24 35
48 63 80
*/

#include<stdio.h>
void main(){
	int rows;
	int num;

	printf("Enter number of rows\n");
	scanf("%d",&rows);

	printf("Enter any number\n");
	scanf("%d",&num);

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d\t",num*num-1);
			num++;
		}
		printf("\n");
	}
}
