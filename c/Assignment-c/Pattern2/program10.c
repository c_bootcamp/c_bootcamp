/*
 D4 C3 B2 A1
 A1 B2 C3 D4
 D4 C3 B2 A1
 A1 B2 C3 D4
*/
#include<stdio.h>
void main(){
	int rows;
	int num;
	char ch;

	printf("enter number of rows\n");
	scanf("%d",&rows);	

	printf("enter any number\n");	
	scanf("%d",&num);	
	
	printf("enter any character\n");	
	scanf(" %c",&ch);	

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			if(i%2 != 0){
				printf("%c%d\t",ch,num);
				ch--;
				num--;
			}else{
				ch++;
				num++;
				printf("%c%d\t",ch,num);
			}
		}
		printf("\n");
	}
}
