//WAP that accepts an array on length N from the user and calculate squares of all even elements and cubes of all odd elements from that array and replaces the elemets respectively with the answer
//i/p: length of array = 6
//enter elements= 1 2 3 4 5 6
//array after operation= 1 4 27 16 125 36

#include<stdio.h>
void main(){
	int size;
	printf("enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements:\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array elements after operations:\n");
	for(int i=0; i<size; i++){
		if(arr[i]%2 == 0 ){
			arr[i] = arr[i] * arr[i];
		}else{
			arr[i] = arr[i] * arr[i] * arr[i];
		}
	}

	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
