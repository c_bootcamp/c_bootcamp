/*
1 4 3
16 5 36
7 64 9
*/

#include<stdio.h>
void main(){
	int rows;
	int num;

	printf("enter number of rows\n");
	scanf("%d",&rows);

	printf("enter any number\n");
	scanf("%d",&num);

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			if((i%2==0&&j%2!=0)||(i%2!=0&&j%2==0)){
				printf("%d\t",num*num);
			}else{
				printf("%d\t",num);
			}
		        num++;
		}
		printf("\n");
       }
}
