/* Thread creation*/

#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

void* fun(){

	printf("in fun\n");
	printf("%ld\n",pthread_self());
	printf("end fun\n");
}

void main(){

	printf("in main\n");

	printf("%ld\n",pthread_self());

	pthread_t tid;

	pthread_create(&tid,NULL,fun,NULL);

	pthread_join(tid,NULL);
	printf("end main\n");
}
