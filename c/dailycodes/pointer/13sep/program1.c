//Basics of pointer

#include<stdio.h>
void main(){
	int x = 10;
	int *ptr = &x;

	printf("%d\n",x);
	printf("%p\n",ptr);	//correct address
	printf("%p\n",&x);	//correct address
	printf("%d\n",&x);	//warning...incomplete address
	printf("%ld\n",&x);	//warning...extra address
	printf("%d\n",ptr);	//warning...incomplete address
}
























































