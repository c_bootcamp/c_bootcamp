//use of dangling pointer in malloc

#include<stdio.h>
#include<stdlib.h>
void fun(int x){
	int *ptr1 =(int*)malloc(sizeof(int));
	ptr1=&x;
	printf("%d\n",*ptr1);
	int *ptr2=ptr1;
	printf("%d\n",*ptr2);                     
}
void main(){
	fun(10);
}

