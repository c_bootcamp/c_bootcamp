//pragma pack-->for not to waste memory

#include<stdio.h>
#pragma pack(1)

struct Demo{

/*	***WITHOUT PRAGMA PACK***
	char ch1;	//+
	int x;		//+ = 8
	float y;	//8
	double arr[5];	//double mhanje 8-8 byte che 5 box i.e. 8*5 = 40 bytes == 52
*/

	char ch1;	//1
	int x;		//4
	float y;	//4
	double arr[5];	//40 == 49
};

void main(){
	printf("%ld\n",sizeof(struct Demo));
}
