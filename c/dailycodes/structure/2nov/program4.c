//take value from user in structure

#include<stdio.h>

struct OTT{
	char pName[20];
	int account;
	float price;
};

void main(){
	
	struct OTT obj1 = {"Amazon prime",5,500.00};

	struct OTT obj2;

	printf("Enter oTT platform name:\n");
	gets(obj2.pName);

	printf("Enter total accounts:\n");
	scanf("%d",&obj2.account);

	printf("Enter price of OTT platform:\n");
	scanf("%f",&obj2.price);

	printf("%s\n",obj1.pName);
	printf("%d\n",obj1.account);
	printf("%f\n",obj1.price);

	printf("%s\n",obj2.pName);
	printf("%d\n",obj2.account);
	printf("%f\n",obj2.price);
}
