//my-example

#include<stdio.h>
#include<stdlib.h>

struct Core2web{
	char LName[20];
	int TDays;
	float fees;
}obj1={"C-DS-Cpp",105,8000.00};

void mystrcpy(char *dest,char *src){
	while(*src != '\0'){
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
}

void main(){
	
	struct Core2web obj2={"java",90,7000.00};

	struct Core2web *ptr =(struct Core2web*)malloc(sizeof(struct Core2web));

	mystrcpy((*ptr).LName,"python");
	(*ptr).TDays=90;
	(*ptr).fees=6000.00;

	printf("Course-Name : %s\n",obj1.LName);
	printf("Course Duration in Days : %d\n",obj1.TDays);
	printf("Course-Fees : %f\n",obj1.fees);

	printf("Course-Name : %s\n",obj2.LName);
	printf("Course Duration in Days : %d\n",obj2.TDays);
	printf("Course-Fees : %f\n",obj2.fees);

	printf("Course-Name : %s\n",(*ptr).LName);
	printf("Course Duration in Days : %d\n",(*ptr).TDays);
	printf("Course-Fees : %f\n",(*ptr).fees);

}
