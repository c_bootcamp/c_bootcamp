//data denya chi 1st method...object create krun {...} mdhe value dyaychi...object initialization
#include<stdio.h>

struct movie{
	char mName[20];
	int noofTick;
	float price;
}obj1={"Kantara",2,300.00};	//data section mdhe jaga milel karn te global aahe

void fun(){
	struct movie obj3 ={"Tiger mela aahe",7,1250.00};	//fun chya stack frame mdhe jaga milel

	printf("%s\n",obj3.mName);
	printf("%d\n",obj3.noofTick);
	printf("%f\n",obj3.price);
}

void main(){
	struct movie obj2 ={"Drishyam",2,450.00};		//main chya stack frame mdhe jaga milel

	fun();

	printf("%s\n",obj1.mName);
	printf("%d\n",obj1.noofTick);
	printf("%f\n",obj1.price);

	printf("%s\n",obj2.mName);
	printf("%d\n",obj2.noofTick);
	printf("%f\n",obj2.price);

}
