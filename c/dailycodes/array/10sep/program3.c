//print array elements divided by 2 and its count

#include<stdio.h>
void main(){
	
	int iarr[5];
	int count=0;
		
	printf("Enter array elements:\n");
	for(int i=0; i<5; i++){
		scanf("%d",&iarr[i]);
	}
	
	printf("Array elements divisible by 2 are:\n");
	for(int i=0; i<5; i++){
		if(iarr[i]%2==0){
			printf("%d\n",iarr[i]);
			count++;
		}
	}
	printf("the count is: %d\n",count);
}
