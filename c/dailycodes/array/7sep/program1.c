// 1-D Array Declaration
	
#include<stdio.h>
void main(){

	int arr[]={1,2,3,4,5};
	int arr1[5]={1,2,3,4,5};
	int arr2[5]={1,2,3};
	int arr3[3]={1,2,3,4,5};	//warning: excess elements in an array initializer

	int arr4[3];

	arr4[0]=10;
	arr4[1]=20;
	arr4[2]=30;
}
