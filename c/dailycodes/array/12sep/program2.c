//comparing array

#include<stdio.h>
void main(){
	
	int size;
	printf("enter array size:\n");
	scanf("%d",&size);

	int arr1[size];
	int arr2[size];

	printf("enter array-1 elements:\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr1[i]);
	}
	printf("enter array-2 elements:\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr2[i]);
	}

	int flag=0;

	for(int i=0; i<size; i++){
		if(arr1[i]==arr2[i]){
			flag=1;
		}else{
			flag=0;
			break;
		}
	}

	if(flag==1){
		printf("Both Arrays are same\n");
	}else{
		printf("Arrays are not same\n");
	}
}
