//Assigning an Array to an array

#include<stdio.h>
void main(){

	int size;
	printf("enter array size:\n");
	scanf("%d",&size);

	int arr1[size];
	int arr2[size];

	printf("enter array-1 elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr1[i]);
	}

	printf("Array-1 elements are:\n");
	for(int i=0; i<size; i++){
		printf("%d\n",arr1[i]);
	}

	printf("Array-2 elements are:\n");
	for(int i=0; i<size; i++){
		arr2[i]=arr1[i];
		printf("%d\n",arr2[i]);
	}
}
