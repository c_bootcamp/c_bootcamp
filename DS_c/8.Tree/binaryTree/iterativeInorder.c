#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct TreeNode{

	int data;
	struct TreeNode* left;
	struct TreeNode* right;
};

struct stackFrame{

	struct TreeNode* btNode;
	struct stackFrame* next;
};

struct stackFrame* top = NULL;

struct TreeNode* createNode(int level){
	
	struct TreeNode* newNode = (struct TreeNode *)malloc(sizeof(struct TreeNode));

	printf("Enter data :\n");
	scanf("%d",&newNode->data);

	char ch;

	level = level +1;
	getchar();
	printf("do you want to add left node at level : %d\n",level);
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		newNode->left = createNode(level);
	}else{
		newNode->left = NULL;
	}

	getchar();
	printf("do you want to add right node at level : %d\n",level);
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		newNode->right = createNode(level);
	}else{
		newNode->right = NULL;
	}
	
	return newNode;
}

void preOrder(struct TreeNode* root){

	if(root != NULL){
		printf("%d\t",root->data);
		preOrder(root->left);
		preOrder(root->right);
	}
}

void inOrder(struct TreeNode* root){

	if(root != NULL){
		inOrder(root->left);
		printf("%d\t",root->data);
		inOrder(root->right);
	}
}
	
void postOrder(struct TreeNode* root){
	if(root != NULL){
		postOrder(root->left);
		postOrder(root->right);
		printf("%d\t",root->data);
	}
}


bool isEmpty(){
	if(top == NULL)
		return true;
	else
		return false;
}

void push(struct TreeNode *root){

	struct stackFrame* newNode = (struct stackFrame*)malloc(sizeof(struct stackFrame));

	newNode->btNode = root;
	newNode->next = top;

	top = newNode;
}

struct TreeNode* pop(){
	struct TreeNode* item = top->btNode;
	struct stackFrame* temp = top;
	top = top->next;
	free(temp);
	return item;
}

void stackInOrder(struct TreeNode * root){	
	if(root == NULL){
		return ;
	}else{
		struct TreeNode *temp = root;
		
		while( !isEmpty() || temp != NULL){
			
			if(temp != NULL){
				push(temp);
				temp = temp->left;
			}else{
				temp = pop();
				printf("%d\n",temp->data);		
				temp = temp->right;
			}
		}
	}
}

void main(){

	printf("Let's make binary tree..!!\n");

	char ch;
	
	struct TreeNode* root = (struct TreeNode *)malloc(sizeof(struct TreeNode));

	printf("Enter data :\n");
	scanf("%d",&root->data);

	printf("\t\t\t\tTree is rooted by %d \n",root->data);
	
	getchar();
	
	int level = 0;

	printf("do you want to add left node at root : \n");
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		root->left = createNode(level);
	}else{
		root->left = NULL;
	}

	getchar();
	
	printf("do you want to add right node at root :\n");
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		root->right = createNode(level);
	}else{
		root->right = NULL;
	}

	printf("\nlet's print tree..!!\n");

	do{

		printf("1.preOrder :\n");
		printf("2.inOrder :\n");
		printf("3.postOrder :\n");
		printf("3.stackInOrder :\n");

		int choice;

		printf("enter your choice :\n");
		scanf("%d",&choice);

		switch(choice){

			case 1 :
			       	 preOrder(root);
				 break;
			
			case 2 :
				 inOrder(root);
				 break;
			
			case 3 :
			       	 postOrder(root);
			 	 break;

			case 4 :
			       	 stackInOrder(root);
				 break;

			default : printf("wrong choice..!!\n");
				  break;

		}

		getchar();
		printf("\ndo you want to continue...y/n !!\n");
		scanf("%c",&ch);

	}while(ch == 'y' || ch == 'Y');
}
