#include<stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode* left;
	struct TreeNode* right;
};

struct TreeNode* createNode(int level){

	struct TreeNode* newNode = (struct TreeNode*) malloc(sizeof(struct TreeNode));

	printf("enter data :\n");
	scanf("%d", &newNode->data);

	getchar();

	level = level +1;
	char ch;

	printf("do you want to add left node at level %d...y/n\n",level);
	scanf("%c",&ch);

	if(ch =='y' || ch == 'Y')
		newNode->left = createNode(level);
	else
		newNode->left = NULL;
	
	getchar();

	printf("do you want to add right node at level %d...y/n\n",level);
	scanf("%c",&ch);

	if(ch =='y' || ch == 'Y')
		newNode->right = createNode(level);
	else
		newNode->right = NULL;
	
	return newNode;
}

void preOrder(struct TreeNode* root){

	if(root == NULL)
		return ;
	
	printf("%d\t",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void inOrder(struct TreeNode* root){

	if(root == NULL)
		return ;
	
	inOrder(root->left);
	printf("%d\t",root->data);
	inOrder(root->right);
}
void postOrder(struct TreeNode* root){

	if(root == NULL)
		return ;
	
	postOrder(root->left);
	postOrder(root->right);
	printf("%d\t",root->data);
}

void main(){

	printf("lets make binary tree..!!\n");

	struct TreeNode* root = (struct TreeNode*) malloc(sizeof(struct TreeNode));

	printf("enter data :\n");
	scanf("%d",&root->data);

	printf("Tree is rooted by %d \n",root->data);
		
	int level = 1;
	char ch ;

	getchar();

	printf("do you want to add left node at level %d...y/n\n",level);
	scanf("%c",&ch);

	if(ch =='y' || ch == 'Y')
		root->left = createNode(level);
	else
		root->left = NULL;
	
	getchar();

	printf("do you want to add right node at level %d...y/n\n",level);
	scanf("%c",&ch);

	if(ch =='y' || ch == 'Y')
		root->right = createNode(level);
	else
		root->right = NULL;

	char var;

	do{

		printf("1. PreOrder Traverser\n");
		printf("2. InOrder Traverser\n");
		printf("3. PostOrder traverser\n");

		int choice;
		printf("Enter your choice :\n");
		scanf("%d",&choice);

		switch(choice){

			case 1 :
				preOrder(root);
				break;
			case 2 :
				inOrder(root);
				break;
			case 3 :
				postOrder(root);
				break;
			default :
				printf("wrong choice..!!");
				break;
		}

		getchar();
		printf("\ndo you want to continue..y/n\n");
		scanf("%c",&var);

	}while(var == 'y' || var == 'Y');
}
