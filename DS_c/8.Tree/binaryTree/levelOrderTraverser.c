//traverser types : 1.preOrder 2.inOrder 3.postOrder 4.iterativeInOrder  5.levelOrder


#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct TreeNode{

	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct StackFrame{

	struct TreeNode *btNode;
	struct StackFrame *next;
};

struct Queue{

	struct TreeNode *btNode;
	struct Queue *next;
};

struct StackFrame *top = NULL;
struct Queue *front = NULL;
struct Queue *rear = NULL;

void preOrder(struct TreeNode* root){

	if(root != NULL){

		printf("%d\t",root->data);
		preOrder(root->left);
		preOrder(root->right);
	}
}

void inOrder(struct TreeNode* root){

	if(root != NULL){

		inOrder(root->left);
		printf("%d\t",root->data);
		inOrder(root->right);
	}
}

void postOrder(struct TreeNode* root){

	if(root != NULL){

		postOrder(root->left);
		postOrder(root->right);
		printf("%d\t",root->data);
	}
}

/* iterativeInOrder traversal (using stack): isEmpty() , push() , pop() , iterativeInOrder() */

bool isEmpty(){

	if(top == NULL)
		return true;
	else
		return false;
}

void push(struct TreeNode *temp){

	struct StackFrame *newNode = (struct StackFrame*)malloc(sizeof(struct StackFrame));
	
	newNode->btNode = temp;
	newNode->next = top;

	top = newNode;
}

struct TreeNode* pop(){

	if(isEmpty()){
		printf("stack is empty..!\n");
	}else{
		struct StackFrame *temp = top;
		struct TreeNode *item = temp->btNode;

		top = top->next;
		free(temp);
		return item;
	}
}

void iterativeInOrder(struct TreeNode *root){

	struct TreeNode *temp = root;

	while(!isEmpty() || temp != NULL){

		if(temp != NULL){
			push(temp);
			temp = temp->left;
		}else{

			temp = pop();
			printf("%d\n",temp->data);
			temp = temp->right;
		}
	}
}

/* levelOrder traversal (using queue): isQueueEmpty() , enqueue() , dequeue() , levelOrder() */

bool isQueueEmpty(){

	if(front == NULL && rear == NULL){
		return true;
	}else{
		return false;
	}
}

void enqueue(struct TreeNode * temp){

	struct Queue *newNode = (struct Queue*)malloc(sizeof(struct Queue));

	newNode->btNode = temp;
	newNode->next = NULL;
	
	if(isQueueEmpty()){
		front = rear= newNode;
	}else{	
		rear->next = newNode;
		rear = newNode;
	}
}

struct TreeNode* dequeue(){

	if(isQueueEmpty()){
		printf("tree is empty..!!\n");
	}else{
		struct Queue* temp = front;
		struct TreeNode* item = front->btNode;

		if(front == rear){
			front = rear = NULL;
		}else{
			front = front->next;
		}
		free(temp);
		return item;
	}
}

void levelOrder(struct TreeNode* root){

	struct TreeNode* temp = root;
	enqueue(root);

	while(!isQueueEmpty()){

		temp = dequeue();
		printf("%d\t",temp->data);

		if(temp->left != NULL){
			enqueue(temp->left);
		}
		if(temp->right != NULL){
			enqueue(temp->right);
		}
	}
}

/* creating tree-node*/

struct TreeNode* createNode(int level){

	struct TreeNode *newNode = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	printf("Enter data :\n");
	scanf("%d",&newNode->data);

	getchar();

	char ch;
	printf("Do you want to add left node at level %d :\n",level);
	scanf("%c",&ch);

	if(ch =='y' || ch =='Y'){
		newNode->left = createNode(level+1);
	}else{
		newNode->left = NULL;
	}

	getchar();

	printf("Do you want to add right node at level %d :\n",level);
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		newNode->right = createNode(level+1);
	}else{
		newNode->right = NULL;
	}

	return newNode;
}

void main(){

	printf("lets make a binart tree..!!\n");

	struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	printf("Enter data :\n");
	scanf("%d",&root->data);

	int level = 1;
	char ch;

	getchar();

	printf("do you want to add left node at root node :\n");
	scanf("%c",&ch);

	if(ch =='y' || ch=='Y'){
		root->left = createNode(level);
	}else{
		root->left = NULL;
	}

	getchar();

	printf("do you want to add right node at root node :\n");
	scanf("%c",&ch);

	if(ch =='y' || ch=='Y'){
		root->right = createNode(level);
	}else{
		root->right = NULL;
	}

	printf("let's print the Binary tree..!!\n");

	do{
		printf("1.preOrder()\n");
		printf("2.inOrder()\n");
		printf("3.postOrder()\n");
		printf("4.iterativeInOrder()\n");
		printf("5.levelOrder()\n");

		int choice;

		printf("Enter your choice :\n");
		scanf("%d",&choice);

		switch(choice){

			case 1 : preOrder(root);
				 break;
			case 2 : inOrder(root);
				 break;
			case 3 : postOrder(root);
				 break;
			case 4 : iterativeInOrder(root);
				 break;
			case 5 : levelOrder(root);
				 break;
			default : printf("Wrong choice..!!\n");
				  break;
		}

		getchar();
		printf("\n do you want to continue..y/n!!\n");
		scanf("%c",&ch);

	}while(ch == 'y' || ch == 'Y');
}
