/*Construct a Binart Tree from a 'inOrder' and 'preOrder' traverser array*/

#include<stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode* left;
	struct TreeNode* right;
};

struct TreeNode* ConstructBT(int inOrder[], int preOrder[] ,int inStart,int inEnd,int preStart,int preEnd){

	if(inStart > inEnd){
		return NULL;
	}

	int rootData = preOrder[preStart];

	struct TreeNode* temp =(struct TreeNode*) malloc(sizeof(struct TreeNode));
	temp->data = rootData;

	int idx;
	for(idx = inStart; idx <= inEnd; idx++){
		if(rootData == inOrder[idx])
			break;
	}

	int preLen = idx - inStart;
	
	temp->left = ConstructBT(inOrder,preOrder,inStart,idx-1,preStart+1,preStart+preLen);
	temp->right = ConstructBT(inOrder,preOrder,idx+1,inEnd,preStart+preLen+1,preEnd);

	return temp;

}

void preOrderBT(struct TreeNode* root){

	if(root == NULL)
		return;

	printf("%d\t",root->data);
	preOrderBT(root->left);
	preOrderBT(root->right);
}

void inOrderBT(struct TreeNode* root){

	if(root == NULL)
		return;

	inOrderBT(root->left);
	printf("%d\t",root->data);
	inOrderBT(root->right);
}

void postOrderBT(struct TreeNode* root){

	if(root == NULL)
		return;

	postOrderBT(root->left);
	postOrderBT(root->right);
	printf("%d\t",root->data);
}

void main(){

	int inOrder[] = {4,2,5,1,6,7,3};
	int preOrder[] = {1,2,4,5,3,6,7};

	int size = sizeof(inOrder)/sizeof(inOrder[0]);

	int inStart =0, inEnd = size-1, preStart = 0, preEnd = size-1;
	struct TreeNode* root = ConstructBT(inOrder,preOrder,inStart,inEnd,preStart,preEnd);

	printf("let's print the Binary Tree..!!\n");

	printf("preOrder : \n");
	preOrderBT(root);
	
	printf("\ninOrder : \n");
	inOrderBT(root);
	
	printf("\npostOrder : \n");
	postOrderBT(root);

	printf("\n");
}
