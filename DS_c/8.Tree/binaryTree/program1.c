//tree cha 1st code..trial

#include<stdio.h>
#include<stdlib.h>

struct node* root = NULL;
struct node{
	int data;
	struct node* left;
	struct node* right;
};

void preorder(struct node* root){
	if(root != NULL){
		printf("%d\t",root->data);
		preorder(root->left);
		preorder(root->right);
	}
}

void inorder(struct node* root){
	if(root != NULL){
		inorder(root->left);
		printf("%d\t",root->data);
		inorder(root->right);
	}
}

void postorder(struct node* root){
	if(root != NULL){
		postorder(root->left);
		postorder(root->right);
		printf("%d\t",root->data);
	}
}

struct node* createNode(){

	struct node* newnode = (struct node*)malloc(sizeof(struct node));

	printf("enter data :\n");
	scanf("%d",&newnode->data);

	if(root == NULL)
		root = newnode;
	
	char ch;

	printf("do you want to add left-node to %d ?..y/n\n",newnode->data);
	scanf(" %c",&ch);
	if(ch == 'y' || ch == 'Y')
		newnode->left = createNode();
	else
		newnode->left = NULL;

	printf("do you want to add right-node to %d ?..y/n\n",newnode->data);
	scanf(" %c",&ch);
	if(ch == 'y' || ch == 'Y')
		newnode->right = createNode();	
	else
		newnode->right = NULL;

	return newnode;
}

void main(){

	createNode();
	printf("tree is :\n");
	printf("Preorder is : ");
	preorder(root);
	printf("\n");
	printf("Inorder is : ");
	inorder(root);
	printf("\n");
	printf("Postorder is : ");
	postorder(root);
	printf("\n");
	printf("root is : %d\n",root->data);
}
