#include<stdio.h>

void makingBT(int inArr[], int preArr[], int start, int end, int istart, int iend){

	int root = preArr[start];
	int index = -1;

	for(int i= 0; i<end; i++){
		if(inArr[i] == root){
			index = i;
			break;
		}
	}
	if(istart > iend)
		return;
	
	printf("%d\n",root);
	makingBT(inArr,preArr,++start,end,index-1,iend);
	makingBT(inArr,preArr,++start,end,index+1,iend);
}

void main(){

	int inArr[] = {2,5,4,1,6,3,8,7};
	int preArr[] = {1,2,4,5,3,6,7,8};

	int size = sizeof(inArr)/sizeof(inArr[0]);
	makingBT(inArr,preArr,0,size-1,0,size-1);
}
