/* WaP that accepts two singly linear linked list from the user and concat source linked list after destination linked list  */

#include<stdio.h>
#include<stdlib.h>

struct node{

	int no;
	struct node *next;
};


struct node *head1 = NULL;
struct node *head2 = NULL;

struct node* createNode(){

	struct node *newNode = (struct node *) malloc(sizeof(struct node));

	printf("enter data :\n");
	scanf("%d",&newNode->no);

	struct node *next = NULL;

	return newNode;
}


void addNode(struct node **head){

	struct node *newNode = createNode();

	if(*head == NULL)
		*head = newNode;
	else{

		struct node *temp = *head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(struct node **head){

	struct node *temp = *head;
	while(temp->next != NULL){
		printf("|%d|->",temp->no);
		temp = temp->next;				
	}
	printf("|%d|\n",temp->no);
}


void main(){
	
	int x,y;
	printf("number of nodes for LL-1 :\n");
	scanf("%d",&x);

	for(int i=1; i<= x; i++){
		addNode(&head1);
	}

	printf("number of nodes for LL-2 :\n");
	scanf("%d",&y);

	for(int i=1; i<= y; i++){
		addNode(&head2);
	}	

	printf("LL-1 :\n");
	printLL(&head1);

	printf("LL-2 :\n");
	printLL(&head2);

	printf("Resultant LL after concating source LL (LL-1) after destination LL (LL-2) :\n");

	struct node *temp = head2;

	while(temp->next != NULL){
		temp = temp->next;
	}

	temp->next = head1;

	printf("LL-1 is :\n");
	printLL(&head1);

	printf("LL-2 is :\n");
	printLL(&head2);
}
