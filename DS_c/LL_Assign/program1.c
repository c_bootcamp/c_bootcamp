/* WaP that searches all occurances of a particular element from a singly linear linked list */

#include<stdio.h>
#include<stdlib.h>

struct node{

	int no;
	struct node *next;
};

struct node *head = NULL;

struct node* createNode(){

	struct node *newNode = (struct node *) malloc(sizeof(struct node));

	printf("enter data :\n");
	scanf("%d",&newNode->no);

	struct node *next = NULL;

	return newNode;
}

void addNode(){

	struct node *newNode = createNode();

	if(head == NULL)
		head = newNode;
	else{

		struct node *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

int count(){

	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		int count = 0;
		struct node *temp = head;

		while(temp != NULL){
	
			count++;
			temp = temp->next;
		}
	
		printf("total count of nodes is : %d\n",count);
		return 0;
	}
}

int occurance(int num){

	struct node *temp = head;
	int occr = 0 ;

	while(temp != NULL){
	
		if(temp->no == num){
			occr++;
		}
		temp = temp->next;
	}
	return occr;
}

void main(){

	char choice;

	do{
		printf("1. add new node \n");
		printf("2. count total number of nodes \n");
		printf("3. search no. of occurances of data \n");

		int ch;
		printf("enter your choice :\n");
		scanf("%d",&ch);

		switch (ch) {

			case 1 : addNode();
				 break;

			case 2 : count();
				 break;

			case 3 :
				 {
					 if(head == NULL){
						printf("Linked list is empty!!\n");
					 }else{
						 int num;
						 printf("enter the number to be searched :\n");
						 scanf("%d",&num);

					 	int occr = occurance(num);	
				
					 	if(occr != 0)
					 		printf("%d occurs for %d times\n",num,occr);
					 	else
							 printf("%d is not present in the Linked list\n",num);
					 }
				 }
				 break;

			default : printf("wrong choice..!!\n");
				  break;
		}
		
		getchar();
		printf("do you want to continue...y/n !!\n");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');
}
