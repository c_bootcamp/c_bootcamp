/* WaP that accepts two singly linear LL from the user and concat the last N elements of the source linear LL after the destination LL */

#include<stdio.h>
#include<stdlib.h>

struct node{

	int no;
	struct node *next;
};


struct node *head1 = NULL;
struct node *head2 = NULL;

struct node* createNode(){

	struct node *newNode = (struct node *) malloc(sizeof(struct node));

	printf("enter data :\n");
	scanf("%d",&newNode->no);

	struct node *next = NULL;

	return newNode;
}


void addNode(struct node **head){

	struct node *newNode = createNode();

	if(*head == NULL)
		*head = newNode;
	else{

		struct node *temp = *head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(struct node **head){

	struct node *temp = *head;
	while(temp->next != NULL){
		printf("|%d|->",temp->no);
		temp = temp->next;				
	}
	printf("|%d|\n",temp->no);
}
void main(){
	
	int x,y;
	printf("number of nodes for source LL :\n");
	scanf("%d",&x);

	for(int i=1; i<= x; i++){
		addNode(&head1);
	}

	printf("number of nodes for destination LL :\n");
	scanf("%d",&y);

	for(int i=1; i<= y; i++){
		addNode(&head2);
	}	

	printf("source LL :\n");
	printLL(&head1);

	printf("destination LL :\n");
	printLL(&head2);
	
	int num;
	printf("enter number of nodes for concatination :\n");
	scanf("%d",&num);

	if(num <= x){
		struct node *temp1 = head1;
		struct node *temp2 = head2;

		while(temp2->next != NULL){
			temp2 = temp2->next;
		}

		int count = x - num;
		while(count){
			temp1 = temp1->next;
			count--;	
		}
		
		temp2->next = temp1;

		printf("LL after concating last %d nodes :\n",num);
		printf("source LL :\n");
		printLL(&head1);

		printf("destination(concated) LL :\n");
		printLL(&head2);
	}else{
		printf("wrong input..!!");
	}	
}

