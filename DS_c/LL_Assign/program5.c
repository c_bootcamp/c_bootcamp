/* WaP that accepts two singly linear LL from the user and also accepts range and concat elements of the source singly linear LL from that range after a singly linear destination linked list */

#include<stdio.h>
#include<stdlib.h>

struct node{

	int no;
	struct node *next;
};


struct node *head1 = NULL;
struct node *head2 = NULL;

struct node* createNode(){

	struct node *newNode = (struct node *) malloc(sizeof(struct node));

	printf("enter data :\n");
	scanf("%d",&newNode->no);

	struct node *next = NULL;

	return newNode;
}


void addNode(struct node **head){

	struct node *newNode = createNode();

	if(*head == NULL)
		*head = newNode;
	else{

		struct node *temp = *head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(struct node **head){

	struct node *temp = *head;
	while(temp->next != NULL){
		printf("|%d|->",temp->no);
		temp = temp->next;				
	}
	printf("|%d|\n",temp->no);
}
void main(){
	
	int x,y;
	printf("number of nodes for source LL :\n");
	scanf("%d",&x);

	for(int i=1; i<= x; i++){
		addNode(&head1);
	}

	printf("number of nodes for destination LL :\n");
	scanf("%d",&y);

	for(int i=1; i<= y; i++){
		addNode(&head2);
	}	

	printf("source LL :\n");
	printLL(&head1);

	printf("destination LL :\n");
	printLL(&head2);
	
	int num1,num2;
	printf("enter starting range :\n");
	scanf("%d",&num1);
	printf("enter ending range :\n");
	scanf("%d",&num2);

	struct node *temp1 = head1;
       	
	for(int i=1; i<=num1-1; i++){
		temp1 = temp1->next;
	}

	struct node *temp2 = head2;

	while(temp2->next != NULL){
		temp2 = temp2->next;
	}

	int count = num2 - num1;

	for(int i=1; i<= count+1 ; i++){	//eg. 5-2 = 3 count yeil..pn 2nd position pn print vhayla pahije mhanun +1 kel
		temp2->next  = temp1;
		temp1 = temp1->next;
		temp2 = temp2->next;
	}

	temp2->next = NULL;		//NULL mule head1 la pn head2 evdhich LL read krta yeil..bakichi nahi..mg t temp1 chya help ne free kru karn ti ashili aata aaceesible nahia
	free(temp1);

	printf("concated destination LL using given range is :\n");
	printLL(&head2);
	//printLL(&head1);
}


