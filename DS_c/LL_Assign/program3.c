/* WaP that accepts two singly linear LL from the user and concat the first N elements of the source LL after the destination LL */

#include<stdio.h>
#include<stdlib.h>

struct node{

	int no;
	struct node *next;
};


struct node *head1 = NULL;
struct node *head2 = NULL;

struct node* createNode(){

	struct node *newNode = (struct node *) malloc(sizeof(struct node));

	printf("enter data :\n");
	scanf("%d",&newNode->no);

	struct node *next = NULL;

	return newNode;
}


void addNode(struct node **head){

	struct node *newNode = createNode();

	if(*head == NULL)
		*head = newNode;
	else{

		struct node *temp = *head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(struct node **head){

	struct node *temp = *head;
	while(temp->next != NULL){
		printf("|%d|->",temp->no);
		temp = temp->next;				
	}
	printf("|%d|\n",temp->no);
}
void main(){
	
	int x,y;
	printf("number of nodes for source LL :\n");
	scanf("%d",&x);

	for(int i=1; i<= x; i++){
		addNode(&head1);
	}

	printf("number of nodes for destination LL :\n");
	scanf("%d",&y);

	for(int i=1; i<= y; i++){
		addNode(&head2);
	}	

	printf("source LL :\n");
	printLL(&head1);

	printf("destination LL :\n");
	printLL(&head2);
	
	int num;
	printf("enter number of nodes for concatination :\n");
	scanf("%d",&num);

	struct node *temp2 = head2;
	struct node *temp1 = head1;

	if(num <= x){
		while(temp2->next != NULL){
			temp2 = temp2->next;
		}

		for(int i=1; i<=num ; i++){
			temp2->next = temp1;
			temp1 = temp1->next;
			temp2 = temp2->next;
		}

		temp2->next = NULL;	//source LL pn change hotey

		printf("destination Linked lists after concating :\n");
		printLL(&head2);
	}else{
		printf("wrong choice...!!\n");
	}
}
