/* Counting Sort : T.C. = O(N)
 * 		   S.C > O(1) (extra array gheto)
 * 		   number system nusar code bdlt jato
 * 		   given array mdhe max find kraych aani (max+1) size cha, count thevayla, array ghyaycha or kdhi tri required size pn given aste..
 * 		   array mdhlya pratek element cha count kadhum new array mdhe thevaycha..e.g countArr..aani nantr tyachi cumulative sum kadhun tyach countArr mdhe thevaych...nantr actual sorting sathi oupput[size] cha array ghaycha...correct formula use krun to last la given array bmdhe copy karycha!!
 * 		   jvlchya range mdhle number astil trch use kraych...otherwise dusre sorting use kra (but they have O(N^2) or N.log(N) time complexity aahe )
******************************************************
e.g. {3,7,2,1,8,2,5,2,7} ==>>for +ve integers(different for elements, with -ve values)

*/

#include<stdio.h>

void countArr(int *arr, int size){

	int max = arr[0];

	for(int i=1; i<size; i++){

		if(max < arr[i])
			max = arr[i];
	}

	int carr[max+1];

	//initializer array with zero...
	for(int i=0; i<=max; i++){

		carr[i] = 0;
	}

	//take count of each element from original array in 'carr'(i.e. extra array)
	for(int i=0; i< size; i++){
		carr[arr[i]]++;
	}

	//cumulative sum
	for(int i=1; i<=max; i++){
		carr[i] = carr[i] + carr[i-1];
		//carr[i] += carr[i-1];
	}

	int output[size];
	//stable version of arrray..i.e. same sequence of elements as that of original array(mostly in duplicate elements)
	for(int i=size-1; i>=0; i--){
		output[carr[arr[i]] -1] = arr[i];
		carr[arr[i]]--;
	}

	//copy to original Array
	for(int i=0; i<= size-1; i++){
		arr[i] = output[i];
	}
}

void main(){

	int size;
	printf("enter size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("Array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	
	printf("\n");
	//sorting call (count sort is used)
	countArr(arr,size);

	printf("Sorted array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}

