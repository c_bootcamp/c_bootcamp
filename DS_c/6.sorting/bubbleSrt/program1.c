/* Bubble sort : it keeps the largest element on the last index and sorts in that manner 
                 time complexity : o(n^2)*/

#include<stdio.h>

int bubbleSort(int *arr, int size){
	
	for(int i=0; i<size; i++){

		for(int j=0 ;j<size-1-i; j++){		//maximum element last la jaun thevto..
		
			if(arr[j+1] < arr[j]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main(){

	int size;
	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array size :\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is:\n");

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	printf("sorted array is :\n");
	bubbleSort(arr,size);

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
