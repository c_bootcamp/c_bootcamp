/* Merge sort : better with time complexity than bubble or selection sort	
    		time complexity : n(log n)
		recursion use kraych aahe
		array aadhi single element pryant divide kru..mg tyala pratek stage la sort krun original array mdhe store kraych
*/

#include<stdio.h>

int merge(int *arr, int start, int mid, int end){

	//size of two indexes which are newly taken
	int ele1 = mid-start+1;	
	int ele2 = end-mid;

	int arr1[ele1];
	int arr2[ele2];

	//copying elements from original array to new arrays
	for(int i=0; i<ele1; i++){
		arr1[i] = arr[start + i];
	}

	for(int i=0; i<ele2; i++){
		arr2[i] = arr[mid +1 + i];
	}

	int itr1 = 0 ;		//array 1 aani array 2.. he doghi drr veles change hotay aani tyancha starting index ha 0 ch asel
       	int itr2 = 0 ;
	int itr3 = start;	//ha original array cha index asel...to drr veles 0 nko yayla mhanun start ghetly

	//iterating on original and new arrays and modifying the original array 
	while(itr1 < ele1 && itr2 < ele2){
		if(arr1[itr1] < arr2[itr2]){
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}else{
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	//arr1 amd arr2 already sorted aahet...jrr vrchya while false zalya mule baki value copy vhaychya baki astil(jevha ele1 aani ele2 different astil) mg tya ethe copy hotil jya already sorted form mdhe aahet
	while(itr1 < ele1){		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}
	while(itr2 < ele2){
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

int mergeSort(int *arr, int start, int end){

	if(start < end){	//recursion

		int mid = (start + end)/2;

		mergeSort(arr,start,mid);
		mergeSort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}
}

void main(){

	int size;
	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is:\n");

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	printf("sorted array by merge sort is :\n");
	mergeSort(arr,0,size-1);

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
