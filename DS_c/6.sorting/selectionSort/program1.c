/* Selection sort : smallest element fa first index la yeun bsto
  		    swap nahi krt..tyachya proper index shodhto..aani place krto
                    time complexity : O(n^2)

	    e.g.    4	8	2	1	5	7	3
		    1	2	3	4	5	7	8	
*/

#include<stdio.h>

int selectionSort(int *arr,int size){

	for(int i=0; i<size-1; i++){
		int index = i;
		for(int j =i+1; j<size; j++ ){		//j<size...cause j ha i peksha ek index pudhe asnar aahe..so i la aapn second last index la thambvl aahe...i<size-1
			if(arr[index] > arr[j]){
				index = j;		//for complete iteration of 'j' ...i mdhe smallest element yeun bsel..for one iteration..smallest on i'th position..aapn jo smaalest vattoy tyacha index store krun thevtoy 
			}
		}

		int temp = arr[i];
		arr[i] = arr[index];
		arr[index] =  temp;
	}
}

void main(){

	int size;
	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is:\n");

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	printf("sorted array by selection sort is :\n");
	selectionSort(arr,size);

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
