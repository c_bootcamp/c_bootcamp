/* Insertion sort : array divide hoto 2 parts mdhe..i.e. sorted aani unsorted array
 		    proper index find kurn tya element la tyachya correct position la insert krt 
		    aadhi shift hota elements aani mg store hot
		    time complexity : O(n)^2
*/

#include<stdio.h>

int insertionSort(int *arr, int size){

	for(int i =1; i<size; i++){
		
		int val = arr[i];
		int j= i-1;

		for(	; j>=0 && arr[j] > val; j--){
			arr[j+1] = arr[j];
		}
		arr[j+1] = val;
	}
}

void main(){

	int size;
	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is:\n");

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	printf("sorted array by insertion sort is :\n");
	insertionSort(arr,size);

	for(int i =0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}


