/*
 * Radix sort : O(n*d) where 'n' = size of array and 'd' = number of digits in largest number
 * 		it works on bit/ each digit in each element(i.e. at unit/tens/hundread level)
 * 		not an inplace reverse algo. as it requires extra array/space
 * 		it is a stable sort..maintains relative orderof elements with equal value
 * 		used for large range of elements..which was complex with counting sort
 */

#include<stdio.h>

void radix(int *arr,int size,int pos){

	//initializer array with zero...
	int carr[10] = {0};

	/****OR****
	for(int i=0; i<=9; i++){

		carr[i] = 0;
	}
	*/


	//take count of each element from original array in 'carr'(i.e. extra array)
	for(int i=0; i< size; i++){
		carr[(arr[i]/pos)%10]++;
	}

	//cumulative sum
	for(int i=1; i<=9; i++){
		carr[i] = carr[i] + carr[i-1];
		//carr[i] += carr[i-1];
	}

	int output[size];
	//stable version of arrray..i.e. same sequence of elements as that of original array(mostly in duplicate elements)
	for(int i=size-1; i>=0; i--){
		output[carr[(arr[i]/pos)%10]-1] = arr[i] ;
		carr[(arr[i]/pos)%10]-- ;
	}

	//copy to original Array
	for(int i=0; i<= size-1; i++){
		arr[i] = output[i];
	}

}

void main(){
	int size;
	printf("enter size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("Array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	int max = arr[0];
	for(int i=1; i<=size-1; i++){
		if(max<arr[i])
			max = arr[i];
	}

	/*
	int count =0;
	while(max!=0){
		count++;
		max =max/10;
	}
	
	int pos=1;
	for(int i=1; i<=count; i++){
		radix(arr,size,pos);
		pos = pos*10;
	}
	******OR****(below)*/

	for(int pos=1; max/pos > 0; pos = pos*10){
		radix(arr,size,pos);
	}

	printf("Array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
