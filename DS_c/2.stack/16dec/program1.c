/* Implementing stack using array...my */

#include<stdio.h>

int top = -1;
int size;
int flag = 0;

int push(int *stack){

	if(top == size-1){
		printf("stack overflow\n");
		return -1;
	}else{
		top++;
		printf("enter data:\n");
		scanf("%d",&stack[top]);

		return 0;
	}
}

int pop(int stack[]){

	flag = 0;

	if(top == -1){
		printf("stack underflow\n");
		return -1;
	}else{
		int data = stack[top];
		top--;
		flag = 1;
		return data;
	}
}

int peek(int *stack){

	flag =0;
	if(top == -1){
		printf("stack underflow\n");
		return -1;
	}else{
		int data = *(stack+top);	//array is internally pointer
		flag =1;
		return data;
	}
}

int printStack(int *stack){

	if(top == -1){
		printf("stack underflow..\n");
		return -1;
	}else{
		for(int i =0 ; i<= top; i++){
			printf("%d\t",stack[i]);
		}
		printf("\n");
		return 0;
	}
}

void main() {

	printf("enter array size :\n");
	scanf("%d",&size);

	int stack[size];

	if(size > 0){

		char choice;

		do{

			printf("1.push \n");
			printf("2.pop \n");
			printf("3.peek \n");
			printf("4.print Stack \n");
	
			int ch;
			printf("enter your choice:\n");
			scanf("%d",&ch);

			switch(ch){
				case 1 : push(stack);
					 break;
				case 2 : {
					 int data = pop(stack);
					 if(flag == 1)
						 printf("%d is popped \n",data);
					 }
					 break;
				case 3 : {
					 int data = peek(stack);
					 if(flag == 1)
						 printf("%d\n",data);
					 }
					 break;
				case 4 : printStack(stack);
					 break;
				default : printf("enter valid choice...\n");
					  break;
			}
	
			getchar();
			printf("Do you want to continue...\n");
			scanf("%c",&choice);
			}while(choice == 'y' || choice == 'Y');
	}else{
		printf("invalid size\n");
	}
}
