/* Implementing 2 stacks using Array */

#include<stdio.h>

int size =0;
int top1 =0;
int top2 =0;
int flag =0;
int flag1 = 0;
int flag2 = 0;

int push1(int arr[]){

	if(top2-top1 == 1){
		return -1;
	}else{
		top1++;
		printf("enter data :\n");
		scanf("%d",&arr[top1]);
		return 0;
	}
}

int push2(int arr[]){

	if(top2-top1 == 1){
		return -1;
	}else{
		top2--;
		printf("enter data :\n");
		scanf("%d",&arr[top2]);
		return 0;
	}
}

int pop1(int arr[]){

	if(top1 == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = arr[top1];
		top1--;
		return val;
	}
}

int pop2(int arr[]){

	if(top2 == size){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = arr[top2];
		top2++;
		return val;
	}
}

int printStack(int arr[]){

//stack-1
	if(top1 == -1){
		flag1 = 0;
	}else{
		flag1 = 1;
		printf("stack -1 : ");
		for(int i = 0; i<= top1; i++){
			printf("%d\t",arr[i]);
		}
		printf("\n");
	}
//stack-2
	if(top2 == size){
		flag2 = 0;
	}else{
		flag2 =1;
		printf("stack -2 : ");
		for(int i = size-1; i >= top2; i--){
			printf("%d\t",arr[i]);
		}
		printf("\n");
	}
}

void main(){

	printf("Enter Array size :\n");
	scanf("%d",&size);

	int arr[size];

	top1 = -1;
	top2 = size;

	char choice ;

	do{
		printf("1. push-1\n");
		printf("2. push-2\n");
		printf("3. pop-1\n");
		printf("4. pop-2\n");
		printf("5. print Stack\n");

		int ch;
		printf("enter your choice :\n");
		scanf("%d",&ch);

		switch(ch){

			case 1 : {
				 int ret = push1(arr);
				 if(ret == -1)
					printf("Stack-1 overflow\n");
				 }
				 break;

			case 2 : {
				 int ret = push2(arr);
				 if(ret == -1)
					printf("Stack-2 overflow\n");
				 }
				 break;

			case 3 : {
				 int ret = pop1(arr);
				 if(flag == 0)
					printf("Stack-1 underflow\n");
				 else
					printf("%d popped\n",ret);
				 }
				 break;
		
			case 4 : {
				 int ret = pop2(arr);
				 if(flag == 0)
					 printf("Stack-2 underflow\n");
				 else
					printf("%d popped\n",ret);
				 }
				 break;

			case 5 : {
				 printStack(arr);
				 if(flag1 == 0)
					 printf("Stack-1 underflow\n");
				 if(flag2 == 0)
					 printf("Stack-2 underflow\n");
				 }
				 break;
		
			default : printf("wrong choice\n");
				  break;
		}

		getchar();
		printf("Do you want to continue..y/n\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
