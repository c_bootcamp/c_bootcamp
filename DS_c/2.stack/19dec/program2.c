/* Implementing stack using Linked list */

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;
int countNode =0;
int flag = 0;

struct Node* createNode() {

	struct Node *newNode = (struct Node *) malloc(sizeof(struct Node));
	
	printf("enter the data :\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	 struct Node *newNode = createNode();
	 if(head == NULL){
		head = newNode;
	 }else{
		struct Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	 }
}

int delLast(){

	if(head == NULL){
		return -1;
	}else{		
		int val;
		if(head->next == NULL){
			val = head->data;
			free(head);
			head = NULL;
		}else{
			struct Node *temp = head;
			while(temp->next->next != NULL){
				temp = temp->next ;
			}
			val = temp->next->data;
			free(temp->next);	
			temp->next = NULL;
		}
		return val;
	}
}

int eleCount(){

	int count = 0;
	struct Node *temp = head;
	
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

bool isFull(){
	
	if(eleCount() == countNode){
		return true;
	}else{
		return false;
	}
}

bool isEmpty(){
	int count = eleCount();
	if(count == 0){
		return true;
	}else{
		return false;
	}
}

int push(){

	if(isFull()){
		return -1;
	}else{
		addNode();
		return 0;
	}
}

int pop(){		//actually delete hot...array mdhe nahi hot delete..mhanun top=-1 use krto!!

	flag = 0;
	if(isEmpty()){
		flag =1;
		return -1;
	}else{
		int val = delLast();
		return val;
	}
}

int peek(){

	flag = 0;
	if(isEmpty()){
		flag =1;
		return -1;
	}else{
		struct Node *temp =  head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		int val = temp->data;
		return val;
	}
}

int printStack(){

	if(head == NULL){
		printf("stack underflow\n");
		return -1;
	}else{
		struct Node *temp = head;
		while(temp != NULL){
			printf("%d\t",temp->data);
			temp = temp->next;
		}
		printf("\n");
		return 0;
	}
}

void main(){

	printf("enter node count for stack :\n");
	scanf("%d",&countNode);
	char choice;

	do{

		printf("1. push\n");
		printf("2. pop\n");
		printf("3. peek\n");
		printf("4. print stack\n");

		int ch;
		printf("enter your choice:\n");
		scanf("%d",&ch);

		switch(ch){
   
			case 1 : { 
				 int ret = push();
				 if(ret)
					printf("stack overflow\n");
				 }
				 break;

			case 2 : {
				 int ret = pop();
				 if(flag){
					printf("Stack underflow\n");
				 }else{
					printf("%d is popped \n",ret);
				 }
				 }
				 break;

			case 3 : {
				 int ret = peek();
				 if(flag){
					printf("Stack underflow\n");
				 }else{
					printf("%d\n",ret);		
				 }
				 }
				 break;

			case 4 : printStack();
				 break;

			default : printf("invalid choice..\n");
				  break;
		}

		getchar();
		printf("Do you want to continue...y/n\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
