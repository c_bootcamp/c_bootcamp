/* Implement stack using Array....(proper code) */

#include<stdio.h>
#include<stdbool.h>

int top = -1;
int size = 0;
int flag = 0;

bool isFull(){

	if(top == size-1 ){
		return true;
	}else{
		return false;
	}
}

bool isEmpty(){

	if(top == -1){
		return true;
	}else{
		return false;
	}
}

int push(int stack[]){
	
	if(isFull()){
		flag = 1;
		return -1;
	}else{
		flag =0;
		top++;
		printf("enter data :\n");
		scanf("%d",&stack[top]);
		return 0;
	}
}

int pop(int stack[]){

	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = stack[top];
		top--;
		return val;
	}
}

int peek(int stack[]){

	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		flag =1;
		int val =stack[top];
		return val;
	}
}

int printStack(int *stack){

	if(isEmpty()){
		printf("Stack underflow..\n");
		return -1;
	}else{
		for(int i = 0; i<= top; i++){
			printf("%d\t",stack[i]);
		}		
		printf("\n");
		return 0;
	}
}

void main(){

	printf("enter array size :\n");
	scanf("%d",&size);

	int stack[size];
	char choice;

	do{
		printf("1. push\n");
		printf("2. pop\n");
		printf("3. peek\n");
		printf("4. print the stack\n");

		int ch;
		printf("enter your choice :\n");
		scanf("%d",&ch);

		switch (ch){
			
			case 1 : {
				 push(stack);
				 if(flag)
					printf("Stack overFlow..\n");
				 }
				 break;

			case 2 : {
				 int ret = pop(stack);
				 if(flag == 0){
					printf("Stack underflow..\n");
				 }else{
					printf("%d is popped\n",ret);
				 }
				 }
				 break;

			case 3 : {
				 int ret = peek(stack);
				 if(flag == 0){
					printf("Stack underflow..\n");
				 }else{
					printf("%d\n",ret);
				 }
				 }
				 break;

			case 4 : printStack(stack);
				 break;

			default : printf("wrong choice..!!\n");
				  break;
		}
		getchar();
		printf("Do you want to continue..y/n\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
