/* Implementing stack using Array...
   push()*/

#include<stdio.h>
#include<stdlib.h>

int top = -1;
int arr[5];

void push(int num){

	top++;
	arr[top] = num; 	//arr[top++]=num
}

void main(){

	push(10);
	push(20);
	push(30);
	push(40);
	push(50);

	for(int i= top; i>=0; i--){
		printf("|%d| ",arr[i]);
	}
	printf("\n");
}


