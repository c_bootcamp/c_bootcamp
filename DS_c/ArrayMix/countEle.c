/* count of element : count the no. of ele that have at least 1 ele greater than itself */

#include<stdio.h>

int arrCount(int *arr,int size){

	int count = 0;
	  
	for(int  i= 0; i<size; i++){
		for(int j= 0; j<size; j++){
			if(arr[i] < arr[j]){
				count++;
				break;
			}
		}
	}
	return count;
}

void main(){

	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}
	
	int count = arrCount(arr,size);

	printf("count is : %d\n ",count);
}

