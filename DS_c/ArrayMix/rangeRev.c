//range reverse

#include<stdio.h>

int rangeRev(int *arr, int a, int b){

	while(a < b){
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
		a++;
		b--;
	}
}

void main(){

	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}

	printf("\n");

	int a;
	int b;
	printf("enter start and end for range :\n");
	scanf("%d %d",&a,&b);

	if(a < b && a < size && b < size){
		rangeRev(arr,a,b);
		printf("reversed array in a range is :\n");
		for(int i=0; i<size; i++){
			printf("%d\t",arr[i]);
		}
		printf("\n");
	}else
		printf("invalid range given\n");
}
