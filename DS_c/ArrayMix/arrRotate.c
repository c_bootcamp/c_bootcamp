/* array rotate for number of shifts */

#include<stdio.h>

int rotateArr(int *arr; int size; int rotate){

	for(int i = 0; i< rotate; i++){
		int end = arr[size-1];
		for(int j = size-1; j> 0; j--){
			arr[j] = arr[j-1];
		}
		arr[0] = end;
	}
}

void main(){

	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}
	
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	int shift;
	printf("enter number of shifts :\n");
	scanf("%d",&shift);

	rotateArr(arr,size,shift);

	printf("rotated array is :\n");
	
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
