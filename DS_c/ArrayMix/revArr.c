/* reverse the array*/

#include<stdio.h>

int revArr(int *arr, int size, int *arr1){

	int i = 0;
	int j = size-1;

	while(i < size){

		arr1[i] = arr[j];
		j--;
		i++;
	}
}

void main(){

	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];
	int arr1[size];

	printf("enter elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	revArr(arr,size,arr1);

	printf("reverse array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr1[i]);
	}
	printf("\n");
}
