/* good pair : */

#include<stdio.h>

int goodPair(int *arr,int size,int key){

	for(int i=0;i<size-1;i++){
		for(int j=i+1; j<size; j++ ){
			if((arr[i]+arr[j]) == key){
				return 1;
			}
		}
	}
	return 0;
}

void main(){

	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key :\n");
	scanf("%d",&key);

	int ret = goodPair(arr,size,key);

	if(ret == 1)
		printf("good pair exist \n");
	else
		printf("good pair does not exist \n");
}
