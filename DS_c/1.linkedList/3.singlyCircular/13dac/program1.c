/* Singly Circular Linked list */

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node* createNode(){

	struct Node *newNode = (struct Node *) malloc (sizeof(struct Node));

	printf("enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	
	return newNode;
}

int addNode(){

	struct Node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		head ->next = head;	// newNode->next =head;
	}else{
		struct Node *temp = head;
		while(temp->next != head){
			temp =temp->next;
		}
		temp->next = newNode;
		newNode->next = head;
	}
}

int countNode(){

	int count =0;
	if(head == NULL){
		printf("Linked list is empty!!");
	}else{
		struct Node *temp = head;
		while(temp->next != head){
			count++;
			temp =temp->next;
		}
		count++;
	}
	return count;
}

int addFirst(){

	struct Node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		head->next = head;
	}else{
		struct Node *temp = head;
		while(temp->next != head){
			temp = temp->next;
		}
		newNode->next = head;
		temp->next = newNode;
		head = newNode;
	}
}

int addAtPos(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count+1){
		printf("invalid position!\n");
		return -1;
	}else{
		if(pos == 1){
			addFirst();
		}else if(pos == count+1){
			addNode();
		}else{
			struct Node *newNode = createNode();
			struct Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}

int delFirst(){
	
	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			struct Node *temp = head;
			while(temp->next != head){
				temp = temp->next;
			}
			head = head->next;
			free(temp->next);
			temp->next = head;
		}
		return 0;
	}
}

int delLast(){

	if(head == NULL){
		printf("Linked list is empty!\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			struct Node *temp = head;
			while(temp->next->next != head){
				temp = temp->next;
			}
			free(temp->next);
			temp->next = head;
		}
		return 0;
	}
}

int delAtPos(int pos){

	int count = countNode();

	if(pos <= 0 || pos > count){
		printf("Invalid position!\n");
		return -1;
	}else{
		if(pos == 1)
			delFirst();
		else if(pos == count){
			delLast();
		}else{
			struct Node *temp1 = head;
			struct Node *temp2 = head;
			while(pos-2){
				temp1 = temp1->next;
			}
			temp2 = temp1->next;
			temp1->next = temp2->next;
			free(temp2);
		}
		return 0;
	}
}

int printLL(){

	if(head == NULL){
		printf("Linked list is empty!!\n");
		return -1;
	}else{
		struct Node *temp = head;
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}

void main(){

	char ch;

	do{

		printf("1. add Node\n");
		printf("2. add First\n");
		printf("3. add at position\n");
		printf("4. delete first\n");
		printf("5. delete last\n");
		printf("6. delete at position\n");
		printf("7. count Node\n");
		printf("8. print Linked list\n");

		int choice;
		printf("enter your choice :\n");
		scanf("%d",&choice);

		switch (choice) {
			
			case 1 : addNode();
				 break;
			case 2 : addFirst();
				 break;
			case 3 : {
					 int pos;
					 printf("enter the position :\n");
					 scanf("%d",&pos);
					 addAtPos(pos);
				 }
				 break;
			case 4 : delFirst();
				 break;
			case 5 : delLast();
				 break;
			case 6 : {
					 int pos;
					 printf("enter the position :\n");
					 scanf("%d",&pos);
					 delAtPos(pos);
				 }
				 break;
			case 7 : {
					int count =  countNode();
					printf("count is : %d\n",count);
				 }
				 break;
			case 8 : printLL();
				 break;
			default :
				printf("wrong choice!!\n");
				break;
		}
		getchar();
		printf("Do you want to continue!!...y/n\n");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}
