//****** addAtPos() ***** [createNode() addNode() addFirst() addAtPos() countNode() printLL()]

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{

	char name[20];
	int id;
	struct Employee *next;
}emp;

emp *head = NULL;

emp * createNode(){

	emp *newNode = (emp *)malloc(sizeof(emp));

	getchar();

	int i =0;
	char ch;

	printf("Enter name of employee :\n");

	while((ch=getchar()) != '\n'){

		(* newNode).name[i] = ch;
		i++;
	}

	printf("Enter employee ID :\n");
	scanf("%d",&newNode->id);

	newNode -> next = NULL;

	return newNode;
}

void addNode(){

	emp *newNode = createNode();

	if(head == NULL){

		head = newNode;
	}else{
		emp *temp = head;

		while(temp->next != NULL){

			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void addFirst(){

	emp *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{

		newNode->next = head;
	
		head = newNode;
	}
}

void addAtPos(int pos){
	
	emp *newNode = createNode();

	emp *temp = head;

	while(pos-2){
	
		temp = temp->next;
		pos--;
	}

	newNode->next = temp->next;
	temp->next = newNode;
}

void printLL(){

	emp *temp = head;

	while(temp != NULL){

		printf("|%s->",temp->name);

		printf("%d|",temp->id);

		temp = temp->next;
	}
}

void countNode(){

	emp *temp = head;
	int count=0;

	while(temp != NULL){

		count++;

		temp = temp->next;
	}

	printf("count : %d\n",count);
}

void main(){

	int nodeCount;
	int pos;

	printf("enter count of Nodes :\n");
	scanf("%d",&nodeCount);

	for(int i=1; i<= nodeCount; i++){

		addNode();
	}

	printf("add node at first :\n");

	addFirst();

	printf("enter position number :\n");
	scanf("%d",&pos);

	addAtPos(pos);

	printLL();

	printf("\n");

	countNode();
}
