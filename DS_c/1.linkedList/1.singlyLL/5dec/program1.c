//addNode() addFirst() addAtPos() count() deleteLast() deleteFirst() printLL()
#include<stdio.h>
#include<stdlib.h>
int countNode();

struct Demo{

	int data;
	struct Demo *next;
};

struct Demo *head = NULL;

struct Demo* createNode(){

	struct Demo * newNode = (struct Demo *) malloc (sizeof(struct Demo));

	printf("Enter data :\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Demo *newNode = createNode();

	if(head == NULL){

		head = newNode;
	}else{
		
		struct Demo * temp = head;

		while(temp->next != NULL ){

			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void addFirst(){

	struct Demo *newNode = createNode();
	
	if(head == NULL){
	
		head = newNode;
	}else{
		newNode->next = head;
		head= newNode;
	}
}

int addAtPos(int pos){
	
	int count = countNode();

	if(pos <= 0 || pos >= count+2){

		printf("Invalid node position\n");
		return -1;
	}else{
		if(pos == count+1){
			addNode();
		}else if(pos == 1){
			addFirst();
		}else{
			struct Demo *newNode = createNode();
			struct Demo *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}

int countNode(){

	struct Demo *temp = head;
	int count =0;

	while(temp != NULL){
		temp = temp->next;
		count++;
	}
	printf("count is :%d\n",count);
	return count;
}

void printLL(){

	struct Demo *temp =head;

	while(temp != NULL){
		printf("|data is: %d|",temp->data);
		temp=temp->next;
	}
}

void deleteFirst(){

	struct Demo *temp = head;
	head = temp->next;
	free(temp);
}

void deleteLast(){

	struct Demo *temp =head;

	while(temp->next->next != NULL){
		temp = temp->next;
	}
	free(temp->next);
	temp->next = NULL;
}

void main(){

	char choice;

	do{
		printf("1.addNode()\n");
		printf("2.addFirst()\n");
		printf("3.addAtPos()\n");
		printf("4.countNode()\n");
		printf("5.printLL()\n");
		printf("6.deleteFirst()\n");
		printf("7.deleteLast()\n");

		int ch;
		printf("Enter funtion number :\n");
		scanf("%d",&ch);

		switch(ch){

			case 1 : addNode();
				 break;
			case 2 : addFirst();
				 break;
			case 3 : {
					int pos;
					printf("Enter position number :\n");
					scanf("%d",&pos);
					addAtPos(pos);
					break;
				 }
			case 4 : countNode();
				 break;
			case 5 : printLL();
				 break;
			case 6 : deleteFirst();
				 break;
			case 7 : deleteLast();
				 break;
			default : 
				 printf("Invalid choice\n");
		}
		getchar();
		printf("\nDo you want to continue....y/n :\n");
		scanf("%c",&choice);
	}while(choice=='y' || choice =='Y');
}
