//Real-time example(Apartment)

#include<stdio.h>
#include<stdlib.h>

typedef struct Apartment{

	char aName[20];
	int flatNo;
	float price;
	struct Apartment *next;
}apt;

apt *head = NULL;

apt * createNode(){
	apt *newNode = (apt*) malloc (sizeof(apt));
	
	getchar();
	printf("Enter apartment Name :\n");
	char ch;
	int i=0;

	while((ch = getchar()) != '\n' ){

		(*newNode).aName[i] = ch;
		i++;
	}
	printf("enter flat number :\n");
	scanf("%d",&newNode->flatNo);

	printf("enter price of flat :\n");
	scanf("%f",&newNode->price);

	newNode->next = NULL;
	return newNode;
}

void addNode(){
	apt *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		apt *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next = newNode;
	}
}

void addFirst(){
	apt *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head = newNode;
	}
}

int countNodes(){
	int count = 0;
	apt *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}	
	return count;
}

int addAtPos(int pos){
	int count = countNodes();
	if(pos <= 0 || pos >= count+2){
		printf("Invalid position\n");
	}else{
		if(pos == count+1){
			addNode();
		}else if(pos == 1){
			addFirst();
		}else{
			apt *newNode =createNode();
			apt *temp = head;

			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next= temp->next;
			temp->next= newNode;
		}
		return 0;
	}
}

void deleteFirst(){
	int count = countNodes();
	if(head == NULL){
		printf("Linked List is empty !!\n");
	}else if(count == 1){
		free(head);
		head = NULL;
	}else{
		apt *temp = head;
		head=temp->next;
		free(temp);
	}
}

void deleteLast(){
	int count = countNodes();
	if(head == NULL){
		printf("Linked list is empty !!\n");
	}else{
		if(count==1){
			free(head);
			head = NULL;
		}else{
			apt *temp = head;
			while(temp->next->next != NULL){
				temp = temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	}
}
int delAtPos(int pos){
	int count = countNodes();
	if(pos<= 0 || pos > count){
		printf("invalid position \n");
		return -1;
	}else{
		if(pos == count){
			deleteLast();
		}else if(pos == 1){
			deleteFirst();
		}else{
			apt *temp1 = head;
			apt *temp2 = head;
			while(pos-2){
				temp1 = temp1->next;
				pos --;
			}
			temp2 = temp1->next;
			temp1->next = temp2->next;
			free(temp2);
		}
		return 0;
	}	
}

void printLL(){
	apt *temp = head;
	while(temp != NULL){
		printf("|%s ->",temp->aName);
		printf("%d ->",temp->flatNo);
		printf("%f|",temp->price);
		temp=temp->next;
	}
	printf("\n");
}

void main(){

	char choice;

	do{

		printf("1.Add Node\n");
		printf("2.Add First\n");
		printf("3.Add At Position\n");
		printf("4.Delete First\n");
		printf("5.Delete Last\n");
		printf("6.Delete At Position\n");
		printf("7.Count Nodes\n");
		printf("8.Print Linked List\n");

		int ch;
		printf("Enter choice :\n");
		scanf("%d",&ch);
		
		switch (ch) {

			case 1 : 
				addNode();
				break;
			case 2 : 
				addFirst();
				break;
			case 3 : {
					 int pos;
					 printf("Enter position :\n");
					 scanf("%d",&pos);
					 addAtPos(pos);
				 }
				break;
			case 4 : 
				deleteFirst();
				break;
			case 5 : 
				deleteLast();
				break;
			case 6 : 
				 {
					 int pos;
					 printf("Enter position :\n");
					 scanf("%d",&pos);
					 delAtPos(pos);
				 }
				break;
			case 7 : 
				{
					int count = countNodes();
					printf("count = %d\n",count);
				}
				break;
			case 8 : 
				printLL();
				break;
			default : 
				printf("Invalid choice \n");
		}
		getchar();
		printf("Do you want to continue...y/n :\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
