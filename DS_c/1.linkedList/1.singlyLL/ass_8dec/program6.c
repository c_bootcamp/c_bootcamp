/* Write a program that accepts a singly linked list from user. take a number from the user and print the data of the length of that number. 
 length of kanha=5 */

#include<stdio.h>
#include<stdlib.h>

typedef struct name{

	char pName[20];
	struct name *next;
}name;

name *head= NULL;

name *createNode(){

	name *newNode = (name *)malloc(sizeof(name));
	
	char ch;
	int i=0;

	printf("enter the name:\n");
	while((ch = getchar()) != '\n'){
		(*newNode).pName[i] = ch;
		i++;
	}
	newNode->next = NULL;
	return newNode;
}

void addNode(){
	name *newNode = createNode();

	if(head == NULL){
		head= newNode;
	}else{
		name *temp =head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

int mystrlen(char str[]){
	
	int count=0;
	while(*str != '\0'){
		count++;
		str++;
	}
	return count;
}

int length(int num){
	
	if(num < 0){
		printf("enter valid length..");
		return -1;
	}else{
		if(head == NULL){
			printf("linked list is empty!!");
		}else{
			int flag = 0;
			name *temp = head;
			while(temp != NULL){
				int len = mystrlen(temp->pName);
				if(len == num){
					printf("|%s|",temp->pName);
					flag = 1;
				}
				temp = temp->next;
			}
			if(flag == 0){
				printf("No string with %d length in linked list !!",num);
			}
		}
		return 0;
	}
}
void printLL(){

	if(head == NULL){
		printf("LL is empty\n");
	}
	else{
		name *temp = head;
		while(temp->next !=NULL){

			printf("|%s|->",temp->pName);
			temp = temp->next;
		}
		printf("|%s|",temp->pName);	
	}
	printf("\n");
}

void main(){
	
	int nodes;
	printf("enter number of nodes:\n");
	scanf("%d",&nodes);

	getchar();
	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();
	
	int num;
	printf("enter the length :\n");
	scanf("%d",&num);

	length(num);
	printf("\n");
}

