/* Write a program that accepts a singly linear linked list from the user. take a number from the user and only keep the elements that are equal in length to that number and delete other elements. and print the linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct name{

	char pName[20];
	struct name *next;
}name;

name *head= NULL;
int count;

name *createNode(){

	name *newNode = (name *)malloc(sizeof(name));
	
	char ch;
	int i=0;

	printf("enter the name:\n");
	while((ch = getchar()) != '\n'){
		(*newNode).pName[i] = ch;
		i++;
	}
	newNode->next = NULL;
	return newNode;
}

void addNode(){
	name *newNode = createNode();

	if(head == NULL){
		head= newNode;
	}else{
		name *temp =head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

int mystrlen(char *str){

	int len =0;
	while(*str != '\0'){
		len++;
		str++;
	}
	return len;
}

/*
int delstr(int num){
	
	if(head == NULL){
		printf("Linked list is empty!!");
	}else{
		if(num<= 0){
			printf("enter valid length!!");
		}else{
			int flag = 1;
			name *temp = head;

			int x= 1;

			while(temp != NULL){

				int len = mystrlen(temp->pName);
				if(count == 1 )	{
					if(num != len){
						free(temp);
						head = NULL;
						flag = 0;
						return -1;
					}
				}else{
					if(num != len){
						if(x == 1){
							head = temp->next;
							name *temp1 = temp;
							free(temp1);
							temp= head;
						}else{
							temp->next = temp->next->next;
							free(temp->next);
							flag =0;
						}						
						x++;
					}
				}
			}
			temp = temp->next;
			if(flag)
				printf("No node deleted as there all string are of given length %d !!",num);
		}
	}
}*/

void printUserlenStr(){

	if(head == NULL){
		printf("Linked list is empty!!");
	}else{
		name *temp = head;
		name *temp1 = head;
		int len;
		printf("enter length of string :\n");
		scanf("%d",&len);

		while(temp != NULL){
			temp1 = temp;
			if(len == mystrlen(temp->pName)){
				printf("|%s|",temp->pName);
				temp = temp->next;
			}else{
				temp = temp->next;
				free(temp1->next);
			}
		}
	}
}

void printLL(){

	if(head == NULL){
		printf("LL is empty");
	}
	else{
		name *temp = head;
		while(temp->next !=NULL){

			printf("|%s|->",temp->pName);
			temp = temp->next;
		}
		printf("|%s|",temp->pName);	
	}
	printf("\n");
}

void main(){

	printf("enter number of nodes:\n");
	scanf("%d",&count);
	getchar();

	int i=1;
	while(i<=count){
		addNode();
		i++;
	}
	
	printLL();

	printUserlenStr();
}
