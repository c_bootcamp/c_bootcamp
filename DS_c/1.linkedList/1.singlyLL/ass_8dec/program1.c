/* program 1 : Write a program that searches for the first occurance of a particular element from a singly linear linked list*/

            demo *temp = head;
#include<stdio.h>
#include<stdlib.h>

typedef struct demo{

	int data;
	struct demo *next;
}demo;

demo *head = NULL;

demo * createNode(){

	demo *newNode = (demo *) malloc (sizeof(demo));

	printf("enter data :\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void main(){

	int nodes;
	printf("enter number of nodes :\n");
	scanf("%d",&nodes);

	for(int i=1; i<=nodes; i++){

		addNode();
	}

	if(head == NULL){
		printf("Linked List is empty!!\n");
	}else{
		int num;
		printf("enter the searching data:\n");
		scanf("%d",&num);

		int flag=0;
		int occurance = 0;
		demo *temp = head;

		while(temp != NULL){
			occurance++;
			if(temp->data == num){
				flag=1;
				break;
			}else{
				flag=0;	
			}	
			temp= temp->next;
		}

		if(flag==1){
			printf("First occurance of %d is at %d position\n",num,occurance);
		}else{
			printf("no matching data in linked list!!\n");
		}
	}
}

