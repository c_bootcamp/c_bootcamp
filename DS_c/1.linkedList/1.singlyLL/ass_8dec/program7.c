/* Write a program that accepts a singly linear linked list from the user. Reverse the data elements from the linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct name{

	char pName[20];
	struct name *next;
}name;

name *head= NULL;

name *createNode(){

	name *newNode = (name *)malloc(sizeof(name));
	
	char ch;
	int i=0;

	printf("enter the name:\n");
	while((ch = getchar()) != '\n'){
		(*newNode).pName[i] = ch;
		i++;
	}
	newNode->next = NULL;
	return newNode;
}

void addNode(){
	name *newNode = createNode();

	if(head == NULL){
		head= newNode;
	}else{
		name *temp =head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(){

	if(head == NULL){
		printf("LL is empty");
	}
	else{
		name *temp = head;
		while(temp->next !=NULL){

			printf("|%s|->",temp->pName);
			temp = temp->next;
		}
		printf("|%s|",temp->pName);	
	}
	printf("\n");
}

char * mystrrev(char *str){

	char *temp = str;
	char ch;

	while(*temp != '\0'){
		temp++;
	}
	temp--;
	while(str<temp){
		ch = *temp;
		*temp = *str;
		*str = ch;

		str++;
		temp--;
	}
}

void printRevLL(){

	if(head == NULL){
		printf("Linked list is empty!!");
	}else{
		printf("Reverse linked list is :\n");
		name *temp = head;

		while(temp != NULL){
			mystrrev(temp->pName);
			printf("|%s|",temp->pName);
			temp = temp->next;
		}
	}
}
void main(){
	
	int nodes;
	printf("enter number of nodes:\n");
	scanf("%d",&nodes);

	getchar();
	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();
	printRevLL();
	printf("\n");
}
