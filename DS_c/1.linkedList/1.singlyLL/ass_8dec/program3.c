/* Write a program that searches the occurance of a particular element from a singly linear linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{

	int data;
	struct demo *next;
}demo;

demo *head = NULL;

demo * createNode(){

	demo *newNode = (demo *) malloc (sizeof(demo));

	printf("enter data :\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void search(int num){
	int count = 0;
	demo *temp = head;
	
	while(temp != NULL){
		if(temp->data == num){
			count++;
		}
		temp = temp->next;
	}
	if(count == 1){
		printf("%d occurs only once in a linked list\n",num);
	}else if(count > 1){
		printf("%d occurs %d times in Linked List \n",num,count);
	}else{
		printf("%d not found in a linked list\n",num);
	}
}

void main(){

	int nodes;
	printf("enter number of nodes :\n");
	scanf("%d",&nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	if(head == NULL){
		printf("Linked list is empty!!\n");
	}else{
		int num;
		printf("enter the searching data\n");
		scanf("%d",&num);

		search(num);
	}
}
