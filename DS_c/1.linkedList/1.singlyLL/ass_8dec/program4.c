/* Write a program that adds the  digits of a data elements from a singly linear linked list and changes the data(sum of data elements digits)*/

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{

	int data;
	struct demo *next;
}demo;

demo *head = NULL;

demo * createNode(){

	demo *newNode = (demo *) malloc (sizeof(demo));

	printf("enter data :\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void main(){

	int nodes;
	printf("enter number of nodes :\n");
	scanf("%d",&nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	if(head == NULL){
		printf("Linked list is empty!!\n");
	}else{
		demo *temp = head;

		while(temp != NULL){
			  int sum=0;
			  while(temp->data != 0){
				int rem = temp->data%10;
				sum=sum + rem;
				temp->data = temp->data/10;
			  }
			  temp->data = sum;
			  printf("|%d|",temp->data);
			  temp = temp->next;
		}
	}
	printf("\n");
}
