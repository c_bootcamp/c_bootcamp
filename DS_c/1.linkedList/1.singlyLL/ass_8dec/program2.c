/*Write a program that searches for the second last occurance of a particular element from a singly linear linked list */

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{

	int data;
	struct demo *next;
}demo;

demo *head = NULL;

demo * createNode(){

	demo *newNode = (demo *) malloc (sizeof(demo));

	printf("enter data :\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void search(int num){

	demo *temp = head;
	int x1=0,x2=0;
	int count=0;

	while(temp != NULL){
		count++;
		if(temp->data == num){
			x2=x1;
			x1=count;
		}
		temp = temp->next;
	}

	if(x1 == 0){
		printf("No matching data \n");
	}else{
		if(x2){
			printf("second last occurance of %d is %d position\n",num,x2);
		}else{
			printf("%d occur only once in linked list at %d position\n",num,x1);
		}
	}
}

void main(){

	int nodes;
	printf("enter number of nodes :\n");
	scanf("%d",&nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	if(head == NULL){
		printf("Linked list is empty!!\n");
	}else{
		int num;
		printf("enter the searching data\n");
		scanf("%d",&num);

		search(num);
	}
}
