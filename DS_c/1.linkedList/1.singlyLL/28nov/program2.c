//basic malloc code (take data from user and print the data with functions)

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct movie{

	int count;
	char name[20];
	float price;
	struct movie *next;
}mv;

void getData(mv *ptr){

	printf("Enter count for movie :\n");
	scanf("%d",&ptr->count);

	getchar();

	printf("Enter movie name :\n");
	fgets(ptr->name,15,stdin);

	printf("Enter price of movie ticket :\n");
	scanf("%f",&ptr->price);

}

void printData(mv *ptr){

	ptr->name[strlen(ptr->name)-1] = '\0';	//to solve the problem of fgets...has (\n) in it!!

	printf("count is : %d\n",ptr->count);

	printf("movie Name is : %s\n",ptr->name);

	printf("ticket price is : %f\n",ptr->price);
}

void main(){
	
	mv *m1 = (mv*)malloc(sizeof(mv));
	mv *m2 = (mv*)malloc(sizeof(mv));
	mv *m3 = (mv*)malloc(sizeof(mv));

	m1->next = m2;
	m2->next = m3;
	m3->next = NULL;

	getData(m1);
	getData(m2);
	getData(m3);

	printData(m1);
	printData(m2);
	printData(m3);
}
