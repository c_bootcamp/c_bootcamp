//******SINGLY LINKEDLIST ******
//correct but not proper aaproach

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;	//self referencial structure pointer
};

void main(){

	struct Node *head = NULL;

	//first_Node
	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	newNode->data = 10;
	newNode->next = NULL;

	//connecting first node
	head = newNode;

	//second node
	newNode = (struct Node*)malloc(sizeof(struct Node));

	newNode->data = 20;
	newNode->next = NULL;

	//connecting second node
	head->next = newNode;

	//third node
	newNode = (struct Node*)malloc(sizeof(struct Node));

	newNode->data = 30;
	newNode->next = NULL;

	//connecting third node
	head->next->next = newNode;

}
