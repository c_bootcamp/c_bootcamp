//malloc function use

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct practicalExam{	//not a self referencial structure !!!
	char subName[20];
	int rollNo;
	float score;
}clg;

void main(){
	int *ptr1 = (int *)malloc(sizeof(int));	//eka integer sathi chi 4byte jaga milel
	*ptr1 = 10;
	printf("%d\n",*ptr1);

	clg *ptr2 = (clg*)malloc(sizeof(clg));	//structure chgya size evdhi jaga milali..malloc ne heap section vr jaga milali jithn fkt address return hoto..to ptr2 ya pointer mdhe store kelay

	strcpy((*ptr2).subName ,"spos");
	(*ptr2).rollNo = 7;
	ptr2->score = 9.3;

	printf("Subject_Name : %s\n",ptr2->subName);
	printf("roll number :%d\n",ptr2->rollNo);
	printf("marks obtained :%f\n",ptr2->score);
}
