//use of structure using malloc()

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct OTT{
	char pName[20];
	int userCount;
	float price;
};

void main(){

	struct OTT *ptr = (struct OTT*)malloc(sizeof(struct OTT));    //-->explicit typecasting

	struct OTT *ptr1=malloc(sizeof(struct OTT));		      //-->implicit typecasting

	strcpy(ptr->pName,"primeVideo");
	ptr->userCount = 10000;
	(*ptr).price = 350.00;

	printf("OTT_platform name : %s\n",ptr->pName);
	printf("user count : %d\n",ptr->userCount);
	printf("price : %f\n",ptr->price);

	strcpy(ptr1->pName,"Netflix");
	ptr1->userCount = 15000;
	(*ptr1).price = 200.00;

	printf("OTT_platform name : %s\n",ptr1->pName);
	printf("user count : %d\n",ptr1->userCount);
	printf("price : %f\n",ptr1->price);
}
