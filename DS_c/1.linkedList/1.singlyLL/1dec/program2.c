//correct code of singly linked list (for multiple nodes)

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct movie{

	char mName[20];
	float imdb;
	struct movie *next;

}mv;

mv *head = NULL;

void addNode(){

	mv *newNode = (mv*)malloc(sizeof(mv));

	printf("enter movie name :\n");
	fgets(newNode->mName,15,stdin);

	printf("enter movie rating :\n");
	scanf("%f",&newNode->imdb);

	getchar();

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{

		mv *temp = head;
		while(temp->next  != NULL){
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(){

	mv *temp = head;

	while(temp != NULL){

		temp->mName[strlen(temp->mName)-1]='\0';

		printf("| %s -> ",temp->mName);
		printf("%f |",temp->imdb);
		temp=temp->next;
	}
}

void main(){

	addNode();
	addNode();
	addNode();
	addNode();
	printLL();
	printf("\n");
}
