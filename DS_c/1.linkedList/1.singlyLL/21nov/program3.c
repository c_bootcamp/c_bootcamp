//basic structure codes....with one head pointer

#include<stdio.h>
#include<string.h>

struct Company{
	
	int empCount;
	char cName[20];
	float rev;
	struct Company *next;	//--> self referencial structure pointer..ha nsel tr he ordinary structure asel pn ya pointer mule te self referencial structure zal!!
};

void main(){
	
	struct Company obj1,obj2,obj3;

	struct Company *head;

	head = &obj1;

	head->empCount = 5000;		
	//(*head).empCount = 5000; ==> internally -> (i.e. arrow) he * aani . (i.e. aadhi dereference(*) aani mg value at(.) krt) mhanun ch jat!!
	//obj1.empCount = 5000; ==> pn ethe aapn pointer ne deta fill kruya..malloc mdhe tss ch asel mhanun!!
	strcpy(head->cName,"eQ Technologies");
	head->rev= 7.8;
	head->next =&obj2;

	obj1.next->empCount = 5500;
	strcpy(obj1.next->cName,"Google");
	obj1.next->rev= 8.5;
	obj1.next->next =&obj3;

	obj2.next->empCount = 7000;
	strcpy(obj2.next->cName,"Microsoft");
	obj2.next->rev= 8.3;
	obj2.next->next =NULL;
}

