/* Reverse the doubly linked list --> by using swap */

#include<stdio.h>
#include<stdlib.h>

typedef struct data{
	struct data *prev;
	int num;
	struct data *next;
}data;

data *head = NULL;

data * createNode(){

	data *newNode = (data *) malloc(sizeof(data));

	newNode->prev = NULL;
	printf("enter the number :\n");
	scanf("%d",&newNode->num);
	newNode->next = NULL;

	return newNode;
}

void addNode(){

	data * newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		data * temp = head;
		while(temp->next != NULL){
			temp= temp->next;
		}
		temp->next= newNode;
		newNode->prev = temp;
	}
}

int countNode(){
	int count=0;
	data * temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

int addFirst(){
	data *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}

int addAtPos(int pos){
	int count = countNode();
	if(pos <= 0 || pos > count+1){
		printf("invalid position!!\n");
		return -1;
	}else{
		if(pos == 1){
			addFirst();		
		}else if(pos == count+1){
			addNode();
		}else{
			data *newNode = createNode();
			data *temp = head;

			while(pos -2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next->prev = newNode;
			newNode->prev = temp;
			temp->next = newNode;
		}
		return 0;
	}
}

int delFirst(){

	int count = countNode();
	if(head == NULL){
		printf("Linked List is empty!!\n");
		return -1;
	}else{
		if(count == 1){
			free(head);
			head = NULL;
		}else{
			head = head->next;		//can also be done by using temp pointer
			free(head->prev);
			head->prev = NULL;
		}
		return 0;
	}
}

int delLast(){

	if(head == NULL){
		printf("Linked List is empty\n");
		return -1;
	}else{
		int count = countNode();
		if(count == 1){
			free(head);
			head = NULL;
		}else{
			data *temp = head;
			while(temp->next->next != NULL){
				temp = temp->next;
			}
			free(temp->next);
			temp->next = NULL;
		}
		return 0;
	}
}

int delAtPos(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count){
		printf("invalid position\n");
		return -1;
	}else{
		if(pos == 1){
			delFirst();
		}else if(pos == count){
			delLast();
		}else{
			data *temp =head;		//can also be done by using 2 temp pointers
			while(pos-2){
				temp = temp->next;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
		return 0;
	}
}

int swapLL(){
	
	if(head == NULL){
		printf("Linked list is empty!!\n");
		return -1;
	}else{
		int count = countNode();
		data *temp1 = head;
		data *temp2 = head;
		while(temp2->next != NULL){
			temp2 = temp2->next;
		}
		int num = count/2;
		while(num){
			int temp = temp2->num;
			temp2->num = temp1->num;
			temp1->num = temp;

			temp1 = temp1->next;
			temp2 = temp2->prev;
			num--;
		}
		return 0;
	}
}

int printLL(){

	if(head == NULL){
		printf("Linked list is empty!!\n");	
		return -1;
	}else{
		data *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->num);
			temp = temp->next;
		}
		printf("|%d|",temp->num);
		printf("\n");
		return  0;
	}
}

void main(){

	char ch;
	do{
		printf("1.add Node\n");
		printf("2.add First\n");
		printf("3.add At Position\n");
		printf("4.count Node\n");
		printf("5.print Linked List\n");
		printf("6.delete first\n");
		printf("7.delete last\n");
		printf("8.delete at position\n");
		printf("9.reverse Linked list(permanantly)\n");	//swap...
	
		int choice;
		printf("enter your choice :\n");
		scanf("%d",&choice);

		switch (choice){

				case 1 : addNode();
					 break;
				case 2 : addFirst();
					 break;
				case 3 :{
						 int pos;
						 printf("enter the position:\n");
						 scanf("%d",&pos);
						 addAtPos(pos);
					}
					 break;
				case 4 :{
					       	int count = countNode();
					 	printf("Node count is : %d\n",count);
					}				
					 break;
				case 5 : printLL();
					 break;
				case 6 : delFirst();
					 break;
				case 7 : delLast();
					 break;
				case 8 : {
						 int pos;
						 printf("enter the position:\n");
						 scanf("%d",&pos);
						 delAtPos(pos);
					 }
					 break;
				case 9 : swapLL();
					 break;
				default :
					 printf("invalid choice!!\n");
		}
		getchar();
		printf("do you want to continue or not...y/n\n");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}

