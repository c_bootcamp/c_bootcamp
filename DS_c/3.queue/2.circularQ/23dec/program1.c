/* Implementing Queue by using Array */

#include<stdio.h>

int size = 0;
int front = -1;
int rear = -1;
int flag = 0;

int enqueue(int arr[]){

	if((rear == size-1 && front == 0) || (rear == front-1)){

		return -1;
	}else{

		if(front == -1){
			front++;
		}else{
			if(rear == size-1 && front != 0)
				rear = -1;
		}

		rear++;
		printf("enter data:\n");
		scanf("%d",&arr[rear]);
		return 0;
	}
}

int dequeue(int *arr){

	if(front == -1){
		flag = 1;
		return -1;
	}else{
		flag = 0;
		int val = arr[front];

		if(front == rear){		//ekch data asel tr reset krr...!!
			front = -1;
			rear = -1;
		}else{
			if(front == size-1){	//last data vr asel front tr to front++ houn illegal index la jail..mhanun front la -1 kel..aani khali increment kely (general)
				front = -1;
			}

			front++;		//delete kela data ..(array mdhe actually delete hot nahi..mg aapn to index ignore kru shakto aani nantr to override hou shakto..so that behaved like ki delete zalay)!!
		}
		return val;
	}
}

int frontt(int arr[]){

	if(front == -1){
		flag = 0; 
		return -1;
	}else{

		flag = 1;
		int val = arr[front];
		return val;
	}
}

int printQueue(int arr[]){

	if(front == -1){
		return -1;

	}else if(front <= rear){
		for(int i= front; i<= rear; i++)
			printf("%d",arr[i]);	

	}else{
		if(front > rear)
			for(int i = front; i <= size-1; i++)
				printf("%d\t",arr[i]);
	
			for(int i = 0; i <= rear; i++)
				printf("%d\t",arr[i]);
	}
}

void main(){

	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];
	char choice;

	do{

		printf("1. enqueue\n");
		printf("2. dequeue\n");
		printf("3. frontt\n");
		printf("4. print Queue\n");

		int ch;
		printf("enter your choice :\n");
		scanf("%d",&ch);

		switch (ch) {

			case 1 : {
					 int ret = enqueue(arr);
					 if(ret == -1)
						 printf("Queue full..!!\n");
				 }
				 break;

			case 2 : {
					 int ret = dequeue(arr);
					 if(flag == 1)
						 printf("Queue is empty..!!\n");
				 }

			case 3 : {
					int ret = frontt(arr);
					if(flag == 0)
						printf("Queue is empty\n");
					else
						printf("front : %d\n",ret);
				 }

			case 4 : {
					int ret = printQueue(arr);
					if(ret == -1)
						printf("Queue is empty\n");
				 }

			default : printf("Wrong choice..!!\n");
				  break;
		}

		printf("Do you want to continue..y/n!!\n");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');
}
