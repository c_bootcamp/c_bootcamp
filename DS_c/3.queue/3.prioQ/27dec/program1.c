/* Implementing Linked List like priority queue...own(in one function) 
   priority queue */

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	int prio;
	struct Node *next;
};

struct Node *head = NULL;

struct Node * createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("enter data:\n");
	scanf("%d",&newNode->data);

	do{
		printf("enter priority between 0 - 5 :\n");
		scanf("%d",&newNode->prio);

	}while(newNode->prio < 0 || newNode->prio > 5);

	newNode->next = NULL;

	return newNode;
}

int addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		return -1;
	}else{
		struct Node *temp = head;

		while(temp != NULL){
			if(newNode->prio > head->prio){		//addFirst
				newNode->next = head;
				head = newNode;
				return -1;
			}

			if(newNode->prio < temp->prio){		//addlast
				if(temp->next == NULL){
					temp->next = newNode;
					return -1;
				}
			}

			if(newNode->prio > temp->next->prio){	//addAtPos
				newNode->next = temp->next;
				temp->next = newNode;
				return -1;
			}

			temp = temp->next;
		}
	}
}

int delFirst(){

	if(head == NULL){

		printf("Linked list is empty\n");
		return -1;
	}else{

		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{

			struct Node *temp = head;
			head = head->next;
			free(temp);
		}
		return 0;
	}
}

int printLL(){

	if(head == NULL){
		printf("Queue is empty\n");
		return -1;
	}else{

		struct Node *temp = head;

		while(temp != NULL){
			printf("|data = %d -> prio = %d|",temp->data,temp->prio);
			temp = temp->next;
		}
		printf("\n");
		return 0;
	}
}

void main(){

	char choice;

	do{
		printf("1. add NOde\n");
		printf("2. delete first Node \n");
		printf("3. print LL\n");

		int ch;
		printf("enter choice :\n");
		scanf("%d",&ch);

		switch(ch){
			case 1 : addNode();
				 break;

			case 2 : delFirst();
				 break;

			case 3 : printLL();
				 break;

			default : printf("wrong choice\n");
				  break;

		}

		getchar();
		printf("Do you want to continue..y/n\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}

