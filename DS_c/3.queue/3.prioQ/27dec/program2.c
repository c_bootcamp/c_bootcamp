/* Implementing Linked List like priority queue...(separate function-own) 
   priority queue */

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	int prio;
	struct Node *next;
};

struct Node *head = NULL;
struct Node *newNode = NULL;

struct Node * createNode(){

	newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("enter data:\n");
	scanf("%d",&newNode->data);

	do{
		printf("enter priority between 0 - 5 :\n");
		scanf("%d",&newNode->prio);

	}while(newNode->prio < 0 || newNode->prio > 5);

	newNode->next = NULL;

	return newNode;
}

int countNode(){

	int count = 0;
	struct Node *temp = head;

	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

int addFirst(){

	newNode->next = head;
	head = newNode;
	return 0;
}

int addAtPos(int count){

	struct Node *temp = head;

	while(count-2){			//jya position la add vhayla pahije tich position aahe..mhanun count -2 pryant temp la firv karn temp already 1st gharavr aahe..aani temp la alikde thabvay ch aahe
		temp = temp->next;
		count--;
	}
	
	newNode->next = temp->next;
	temp->next = newNode;
	return 0;
}

int addNode(){	//addNode(i.e.addLast)..addAtPos..addFirst

	struct Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		return -1;

	}else if(newNode->prio > head->prio){
		addFirst();
		return -1;

	}else{
		int count = 0;
		struct Node *temp = head;

		while(temp != NULL){

			count++;
			
			if(newNode->prio > temp->prio){		//temp->next-prio la segmentation yetyy...temp->next= NULL asel tr mhanun..temp->next->prio lihu shakto pn khalcha if (addLAst sathi cha)vr lihava lagel...so last node sathi cha temp->next == NULL asel...mg jr khalcha if vr lihila tr te handle hoil..!!
				addAtPos(count);
				return -1;
			}

			if(newNode->prio < temp->prio){	//i.e. add at Last
				if(temp->next == NULL){
					temp->next = newNode;
					return -1;
				}
			}	

			temp = temp->next;
		}
	}
}

int delFirst(){

	if(head == NULL){

		printf("Linked list is empty\n");
		return -1;
	}else{

		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{

			struct Node *temp = head;
			head = head->next;
			free(temp);
		}
		return 0;
	}
}

int printLL(){

	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{

		struct Node *temp = head;

		while(temp != NULL){
			printf("|data = %d -> prio = %d|",temp->data,temp->prio);
			temp = temp->next;
		}
		printf("\n");
		return 0;
	}
}

void main(){

	char choice;

	do{
		printf("1. add NOde\n");
		printf("2. delete first Node \n");
		printf("3. Node count\n");
		printf("4. print LL\n");

		int ch;
		printf("enter choice :\n");
		scanf("%d",&ch);

		switch(ch){
			case 1 : addNode();
				 break;

			case 2 : delFirst();
				 break;
				 
			case 3 : {
				  int count = countNode();
				  printf("count : %d\n",count);
				 }
				 break;
				  
			case 4 : printLL();
				 break;

			default : printf("wrong choice\n");
				  break;

		}

		getchar();
		printf("Do you want to continue..y/n\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
