/* Implementing Queue using Linked List 
   (not considering size as we r using heap(growable shrinkable) memory..just deal with memory full on heap...but can be done with size too)   
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *front = NULL;
struct Node *rear = NULL;
int flag = 0;

struct Node * createNode(){

	struct Node *newNode = (struct Node *) malloc (sizeof(struct Node));

	if(newNode == NULL){
		printf("Memory Full\n");
		exit(0);		//process terminated....explicitely by us(system call)
	}
	
	printf("enter data :\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

int enqueue(){

	struct Node *newNode = createNode();
	if(front == NULL){
		front = newNode;
		rear = newNode;
	}
	rear->next = newNode;
	rear = newNode;
	return 0;
}

int dequeue(){

	if(front == NULL){
		flag = 1;
		return -1;
	}else{
		flag = 0;
		int val = front->data;		//first node delete honar aahe...aani ticha data return krr
			
		if(front == rear){		//i.e one node only.... front->next == NULL <<<- is also correct
			free(front);
			front = NULL;
			rear = NULL;
		}else{
			struct Node *temp = front;
		
			front = temp->next;
			free(temp);
		}
		return val;
	}
}

int frontt(){

	if(front == NULL){
		flag = 1;
		return -1;
	}else{
		flag = 0;
		int val = front->data;		// front() mdhe fkt data return hoto...node delete nahi hot
		return val;
	}
}

int printQueue(){

	if(front == NULL){
		return -1;
	}else{
		struct Node *temp = front;
		while(temp != NULL){
			printf("%d\t",temp->data);
			temp = temp->next;
		}
		printf("\n");
		return 0;
	}
}

void main(){

	char choice;

	do{

		printf("1. enqueue\n");
		printf("2. dequeue\n");
		printf("3. frontt\n");
		printf("4. print queue\n");

		int ch;
		printf("enter your choice :\n");
		scanf("%d",&ch);
	
		switch(ch) {

			case 1 : enqueue();
				 break;
			case 2 : {
				  int ret = dequeue();
				  if(flag == 1)
				  	  printf("Queue is empty\n");
				  else
					  printf("%d dequeued \n",ret);
				 }
				 break;

			case 3 : {
				  int ret = frontt();
				  if(flag == 1)
				  	  printf("Queue is empty\n");
				  else
					  printf("front : %d \n",ret);
				 }
				 break;

			case 4 : {
				  int ret = printQueue();
				  if(ret == -1)
					  printf("Queue is empty\n");
				 }
				 break;

			default : printf("wrong choice..\n");
				  break;
		}	
	
		getchar();
		printf("Do you want to continue..y/n!!\n");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
