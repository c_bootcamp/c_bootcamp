/* Problem statement ---> concat last n nodes (singly LL)*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head1 = NULL;
struct Node *head2 = NULL;

struct Node * createNode(){

	struct Node *newNode = (struct Node *)malloc (sizeof(struct Node));

	printf("enter the data:\n");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

void addNode(struct Node **head){
	
	struct Node *newNode = createNode();

	if(*head == NULL){
		*head = newNode;	
	}else{
		struct Node *temp = *head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

int countNode(struct Node *head){
	int count =0;
	struct Node *temp = head;

	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

int concat(){
	struct Node *temp = head1;
	while(temp->next != NULL){
		temp = temp->next;
	}
	temp->next = head2;
}

int concatNLL(int num){
	struct Node *temp1 = head1;
	struct Node *temp2 = head2;

	while(temp1->next != NULL){
		temp1 = temp1->next;
	}
	int val = countNode(head2) - num; 	//jya node pasn add kraych aahe tya node vr jail...val ne
	while(val){
		temp2 = temp2->next;
		val--;
	}
	temp1->next = temp2;	
}

int printLL(struct Node *head){
	if(head == NULL){
		printf("Linked list is empty!\n");
		return -1;
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}

void main(){

	int nodeCount;
	printf("enter number of nodes for LinkedList1 :\n");
	scanf("%d",&nodeCount);

	for(int i =1; i<= nodeCount; i++){
		addNode(&head1);
	}

	printf("enter number of nodes for LinkedList2 :\n");
	scanf("%d",&nodeCount);

	for(int i =1; i<= nodeCount; i++){
		addNode(&head2);
	}

	printLL(head1);
	printLL(head2);
/*
	printf("concat for complete nodes:\n");
	concat();
	printLL(head1);
	printLL(head2);*/

	int num;
	printf("enter the number of nodes to concat :\n");
	scanf("%d",&num);
	concatNLL(num);

	printf("first Linked list is:\n");
	printLL(head1);

	printf("second Linked list is:\n");
	printLL(head2);
}


