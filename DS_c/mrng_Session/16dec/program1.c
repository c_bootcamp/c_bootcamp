/* Inplace reverse of doubly Linked list (many approaches can be possible)
   inplace mdhe next pointer change hoto....address change hoto..data nahii
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{
	
	struct Node *prev;
	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node * createNode(){

	struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
		
	newNode->prev = NULL;

	printf("enter the data :\n");
	scanf("%d",&newNode->data);
	
	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

int inplaceRev(){

	if(head == NULL){
		printf("Linked list is empty!\n");
		return -1;
	}else{
		struct Node *temp = NULL;
		while(head->next != NULL){
			
			head->prev = head->next;
			head->next = temp;
			head = head->prev;
			temp = head->prev;
		}
		if(head->next == NULL){
			head->prev = head->next;
			head->next = temp;
		}
		return 0;
	}
}

int printLL(){

	if(head == NULL){
		printf("Linked list is empty!\n");
		return -1;
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}

void main(){

	char ch;
	
	do{
		printf("1. add Node\n");
		printf("2. print Linked list\n");
		printf("3. Inplace reverse\n");

		int choice;
		printf("enter your choice :\n");
		scanf("%d",&choice);

		switch (choice) {
			
			case 1 : addNode();
				 break;

			case 2 : printLL();
				 break;

			case 3 : inplaceRev();
				 break;

			default : printf("wrong choice!\n");
				  break;
		}
		getchar();
		printf("Do you want to continue...y/n\n");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}


