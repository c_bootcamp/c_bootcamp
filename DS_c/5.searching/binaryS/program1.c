/* Binary search : Soretd data asel trch binary search use kru shakto
 		   data divide houn skip hott jato..mid find kraych..compare kraych < > aani tss left or right shift houn baki skip kraych
   		   time complexity : log2(n)
*/

#include<stdio.h>

int binarySearch(int *arr, int size, int key){

	int start = 0;
	int end = size -1;

	while(start <= end){
		int mid = (start + end)/2;		//mid aapn int ghetlay...pn ha formula chukto jevha int chya range baher size aste..tevha dusra formula use kraycha

		if(arr[mid] == key)
			return mid;

		if(arr[mid] < key)
			start = mid+1;

		if(arr[mid] > key)
			end = mid -1;
	}
	return -1;
}

void main(){

	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key :\n");
	scanf("%d",&key);

	int index = binarySearch(arr,size,key);

	if(index == -1)
		printf("%d not present in an array\n",key);
	else
		printf("%d is at %d position\n",key,index);
}
