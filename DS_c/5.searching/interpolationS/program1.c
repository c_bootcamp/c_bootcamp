/* Interpolation Search : hi searching technique fkt *uniformly* sorted(difference same aasel tr) array vrch apply hote
   		          time complexity : O(1)
*/

/*****WRONG CODE *****/

#include<stdio.h>

int interpolation(int *arr, int size, int key){

	int start = 0;
	int end =size-1;
	int index = -1;

	index = start + ((key -arr[start])/(arr[end] - arr[start]) * (end - start));	//FORMULA IS CORRECT BUT GIVVING INCORRECT O/P SOMETIMES

	return index;
}

void main(){
	int size;
	printf("enter size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key :\n");
	scanf("%d",&key);

	int index = interpolation(arr,size,key);

	if(index == -1)
		printf("%d not present in an array\n",key);
	else
		printf("%d is at %d position\n",key,index);
}
