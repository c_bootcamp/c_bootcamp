/* FirstOccr by binary search...*/

int firstOccr(int *arr, int size, int key){

	int start = 0;
	int end = size-1;
	int store = -1;

	while(start <= end){

		int mid = start + end / 2;
	
		if(arr[mid] == key){
			if(arr[i-1] != key){
				return mid;
			}
			end = mid -1;
		}

		if(arr[mid] < key )
			start = mid + 1;

		if(arr[mid] > key)
			end = mid -1;
	}
	return store;
}
void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key value :\n");
	scanf("%d",&key);

	int index = firstOccr(arr,size,key);

	if(index == -1)
		printf("%d not present in an array\n",key);
	else
		printf("first occurance of %d is at index : %d",key,index);
}
