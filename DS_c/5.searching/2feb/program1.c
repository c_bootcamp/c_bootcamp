/* Floor value [same or greatest value among all elements which is nearer to key and it is less than key value...i.e. floor value */

#include<stdio.h>

int floorVal(int *arr, int size, int key){

	int start = 0;
	int end = size-1;
	int store = -1;
	
	if(arr[end] < key)
		return arr[end];

	while(start <= end){

		int mid = start + end / 2;
	
		if(arr[mid] == key)
			return arr[mid];

		if(arr[mid] < key ){
			store = arr[mid];
			start = mid + 1;
		}

		if(arr[mid] > key)
			end = mid -1;
	}
	return store;
}

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key value :\n");
	scanf("%d",&key);

	int floor = floorVal(arr,size,key);

	printf("floor is : %d",floor);
}
