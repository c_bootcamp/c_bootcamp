/* Celing value [same or greatest value among all elements which is nearer to key and it is greater than key value...i.e. floor value */
//not correct

#include<stdio.h>

int flag = 0;

int celingVal(int *arr, int size, int key){

	int start = 0;
	int end = size-1;
	int store = -1;
	int mid;

	if(arr[start] > key)
		return arr[start];

	while(start <= end){

		mid = start + end / 2;
	
		if(arr[mid] == key){
			flag = 1;
			return arr[mid];
		}

		if(arr[mid] < key )
			start = mid + 1;

		if(arr[mid] > key){
			store = arr[mid];
			end = mid -1;
		}
	}
	return store;
}

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key value :\n");
	scanf("%d",&key);

	int celing = celingVal(arr,size,key);

//	if(flag == 0)
//		printf("Celing Value not found \n");
//	else
		printf("Celing is : %d",celing);
}
