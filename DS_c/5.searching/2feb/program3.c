/* Linear search...floor value*/

#include<stdio.h>

int flag = 0;

int floorVal(int *arr, int size, int key){

	int ret = -1;

	if(arr[size-1] < key){
		flag = 1;
		return arr[size-1];
	}

	for(int i= 0; i< size-1 ; i++){

		if(arr[i] == key){
			flag = 1;
			return arr[i];
		}

		if(arr[i] < key && arr[i +1] > key){
			flag = 1;
			return arr[i];
		}
	}
	return ret;
}

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key value :\n");
	scanf("%d",&key);

	int floor = floorVal(arr,size,key);

	if(flag == 0)
		printf("floor Value not found \n");
	else
		printf("floor is : %d\n",floor);
}
