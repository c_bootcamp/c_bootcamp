/* Rotated Array*/

#include<stdio.h>

int rotatedArray(int *arr, int size, int key){

	int num;
	int start = 0;
	int end = size-1;
	int mid;

	for(int i =0; i<size-1 ; i++){

		if(arr[i] > arr[i+1]){
			num = i +1 ;
			break;
		}
	}

	mid = start + end/2;

	if(key<arr[size-1]){

		if(key == arr[num])
			return num;

		start = num +1;

		while(start <= end){
			mid = start + end / 2;
	
		if(arr[mid] == key)
			return mid;

		if(arr[mid] < key ){
			start = mid + 1;
		}

		if(arr[mid] > key)
			end = mid -1;
		}

	}else{
		end = num -1;
		
		while(start <= end){
			int mid = start + end / 2;
	
		if(arr[mid] == key)
			return mid;

		if(arr[mid] < key )
			start = mid + 1;
		

		if(arr[mid] > key)
			end = mid -1;
		}
	}
}

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");
	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	int key;
	printf("enter key value :\n");
	scanf("%d",&key);

	int index = rotatedArray(arr,size,key);

	printf("index is : %d",index);
}
