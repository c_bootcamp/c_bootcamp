/* Linear search : data sorted nsto...ekek element check krava lagto 
  		   Time complexity = O(n)
*/

//firstOccr...lastOccr...secondLastOccr


#include<stdio.h>

int firstOccr(int *arr, int size,int key){

	for(int i= 0; i<size; i++){

		if(key == arr[i])
			return i;
	}

	return -1;
}

int lastOccr(int *arr, int size, int key){

	int lastOccr = -1;

	for(int i=0; i<size; i++){

		if(key == arr[i]){
			lastOccr = i;
		}
	}

	return lastOccr;
}

int secLast(int *arr, int size, int key){

	int secLast = -1;
	int lastEle = -1;

	for(int i=0; i<size; i++){
	
		if(key == arr[i]){
			secLast = lastEle;
			lastEle = i;		
		}
	}

	return secLast;
}

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements :\n");

	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	printf("array is :\n");
	for(int i=0; i<size; i++){

		printf("%d\t",arr[i]);
	}

	printf("\n");

	char choice;
	do{
		printf("1. first occurance\n");
		printf("2. Last occurance\n");
		printf("3. second last occurance\n");

		int ch;
		printf("enter any choice\n");
		scanf("%d",&ch);

		int key;
		printf("enter key :\n");
		scanf("%d",&key);

		switch (ch){

			case 1 : {
					 int x = firstOccr(arr,size,key);
					 if(x == -1)
						 printf("element not found\n");
					 else
						 printf("first occurance of %d is at %d\n",key,x);

				}
				break;
			case 2 : {
					 int x = lastOccr(arr,size,key);
					 if(x == -1)
						 printf("element not found\n");
					 else
						 printf("last occurance of %d is at %d\n",key,x);

				}
				break;
			case 3 : {
					 int x = secLast(arr,size,key);
					 if(x == -1)
						 printf("second last position not present\n");
					 else
						 printf("second last occurance of %d is at %d\n",key,x);

				}
				break;
		}
	
		getchar();
		printf("do you want to continue..y/n !!\n");
		scanf("%c",&choice);

	}while(choice == 'Y' || choice == 'y');
	
}
