/* prefix sum*/

#include<stdio.h>

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];
	printf("enter array elements\n");


	for(int i= 0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is :\n");
	
	for(int i= 0; i<size; i++){
		printf("%d\t",arr[i]);
	}

	printf("\n");
	
	int prefix[size];

	prefix[0] = arr[0];

	for(int i = 1; i<size; i++){
		prefix[i] = prefix[i-1] + arr[i];	
	}

	printf("prefix array is :\n");

	for(int i= 0; i<size; i++){
		printf("%d\t",prefix[i]);
	}

	printf("\n");

	int quere;
	printf("enter number of queres :\n");
	scanf("%d",&quere);

	for(int i = 1; i<= quere; i++){
		int start;
		int end;

		printf("enter start :\n");
		scanf("%d",&start);

		printf("enter end :\n");
		scanf("%d",&end);

		int sum = prefix[end] - prefix[start - 1];

		printf("sum is : %d\n",sum);
	}
}
