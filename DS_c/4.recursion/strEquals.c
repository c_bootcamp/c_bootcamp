//compare 2 strings if equal
//as '\0' is not counted in length of string

#include<stdio.h>
#include<stdbool.h>

int myStrlen(char *arr){

	int count =0 ;

	while(*arr != '\0'){
		count++;
		arr++;
	}
	return count;
}
/*
int myStrlen(char *arr,int count){

	if(*str == '\0')
		return countl;

	return myStrlen(++arr,++count);
}
*/
bool strCompare(char *str1, char *str2){

	if(*str1 != *str2)
		return 0;
	
	if(*str1 == '\0')
		return 1;

	if((*str1 == *str2 ) && strCompare(++str1,++str2))
		return 1;
}

void main(){

	char arr1[20];
	char arr2[20];

	printf("enter string1 :\n");
	gets(arr1);

	printf("enter string2 :\n");
	gets(arr2);

	if(myStrlen(arr1) == myStrlen(arr2)){

		bool ret = strCompare(arr1,arr2);
	
		if(ret)
			printf("both string are equal!!\n");
		else
			printf("strings are not equal!!\n");
	}else{
		printf("String are not equal!!\n");
	}
}

