//recursion basic ==> print numbers in reverse order upto 1

#include<stdio.h>

void fun(int x){

	if(x>0){

		printf("%d\n",x);
		fun(--x);
	}
}

void main(){

	int x=5;

	fun(x);
}
