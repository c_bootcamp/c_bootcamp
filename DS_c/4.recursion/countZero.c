//count zero : count number of zeros in the number

#include<stdio.h>

//without static variable

int countZero(int num,int count){

	if(num==0)
		return count;
	
	if(num%10 == 0)	
		return countZero(num/10,++count);
	else
		return countZero(num/10,count);
}

void main(){

	int num;
	printf("enter any number :\n");
	scanf("%d",&num);

	int count =0;
	
	if(num == 0)
		count =1;
	else	
		count = countZero(num,count);

	if(count==0)
		printf("No zero in %d\n",num);
	else
		printf("%d has %d zeros\n",num,count);
}
