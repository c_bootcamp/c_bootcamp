/*RECURSION IN ARRAY : JITHE JITHE FOR LOOP AAHE TE RECURSION MDHE JAT
 		       RECURSION MDHE VARIABLE LAGT NAHI..CALL CH TSA DYAYCHA*/

#include<stdio.h>

int arrSum(int arr[],int N){

	if(N == 1)
		return arr[0];

	return arr[N-1] + arrSum(arr,N-1);	//arrSum(array,size) jatey..so ek ghar alikdech thambel(i.e. size-1) ...aani te ek ghar aapn 'arr[n-1]' i.e. actual index ne add krtoy...i.e. present element(arr[N-1]) + tya aadhichya sglya ele.(arrSum(arr,N-1)) chi sum = array sum hoil

}

void main(){

	int arr[] = {10,10,10,20,30};

	int size = sizeof(arr)/sizeof(arr[0]);

	int sum = arrSum(arr,size);

	printf("sum of array elements is : %d\n",sum);
}
