//check if the array is sorted or not..if sorted then return true otherwise make it sort

#include<stdio.h>
#include<stdbool.h>

//***Increasing format is followed for sorting***
bool insortedArr(int arr[],int size){

	if(size == 1)
		return 1;

	if(arr[size-2] > arr[size-1])	//array cha previous index aani current index vr kam krel
		return 0;

	if(arr[size-2] <= arr[size-1] && insortedArr(arr,size-1));
		return 1;
}

//***Decreasing format is followed for sorting***
bool desortedArr(int arr[],int size){

	if(size == 1)
		return 1;
	if(arr[size-2] < arr[size-1])	//array cha previous index aani current index vr kam krel
		return 0;

	if(arr[size-2] >= arr[size-1])
		return desortedArr(arr,size-1);
}

void main(){
	
	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	if(size > 0){
		int arr[size];

		printf("enter array elements : \n");
		for(int i=0; i<size; i++){
			scanf("%d",&arr[i]);
		}

		printf("array is :\n");
		for(int i=0; i<size; i++){
			printf("%d\t",arr[i]);
		}
		printf("\n");
	
		bool ret1 = insortedArr(arr,size);
		bool ret2 = desortedArr(arr,size);
	
		if(ret1 || ret2)
			printf("true : Array is sorted!!\n");
		else
			printf("False : Array is not sorted!!\n");
	}else{
		printf("invalide array size!!\n");
	}
}
