//count number of stack frames(if number goes as -ve value == 2,61,938 approx)(half of +ve number stack fames)
//for -ve number..sign bit is aaded(1st bit is 1 if -ve i.e. leftmost bit)

#include<stdio.h>

void fun(int x){

	printf("%d\n",x);
	fun(--x);
}

void main(){

	fun(0);
}
