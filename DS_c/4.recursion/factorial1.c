//factorial code using one variable
//CONSTRAINS : num >=1

#include<stdio.h>

int factNum(int num){

	static int fact = 1;
	fact = fact * num;
	
	if(num != 1)
		factNum(--num);
	else
		return fact;
}

void main(){

	int num;
	printf("enter any number :\n");
	scanf("%d",&num);

	int fact = factNum(num);
	printf("factorial of %d is %d\n",num,fact);
}


