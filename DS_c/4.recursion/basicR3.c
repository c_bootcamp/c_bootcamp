//count number of stack frames(if static(i.e the variable which goes in data section but not accessible out of its scope) == 5,23,772 approx)
//no. of stack in static > no. of stack in global variable(because when stack frame is pushed then many other things also get added along with variables like return address etc.)


#include<stdio.h>

void fun(){

	static int x=0;
	printf("%d\n",++x);
	fun();
}

void main(){

	fun();
}
