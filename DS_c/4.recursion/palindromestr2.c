// PALINDROME STRING (RECURSION) : check if the string is palindrome string or not

#include<stdio.h>
#include<stdbool.h>

bool palStr(char arr[], int start, int end){

	if(arr[start] != arr[end])
		return 0 ;

	if(start >= end)
		return 1 ;

/*	if(arr[start] == arr[end])
		return palStr(arr,start+1,end-1);

	OR

	if(arr[start] == arr[end] && palStr(arr,start+1,end-1))
		return 1 ;

	OR

*/	return palStr(arr,start+1,end-1);	

}

void main(){

	char arr[] = {'m','a','d','a','m'};

	int start = 0;
	int size = sizeof(arr)/sizeof(arr[0]);

	bool ret = palStr(arr,start,size-1);

	if(ret)
		printf("given string is a palindrome string\n");
	else
		printf("given string is not a palindrome string\n");
}
