//Reverse number(without any extra variable)

#include<stdio.h>

int reverseNo(int num,int rev){

	if(num == 0)
		return rev;

	return reverseNo(num/10,rev*10+num%10);
}

void main(){

	int num;
	printf("enter any number :\n");
	scanf("%d",&num);

	int rev = 0;

	rev = reverseNo(num,rev);

	printf("reverse of %d is %d \n",num,rev);
}
