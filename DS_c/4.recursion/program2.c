//sum of 1-10 using recursion

#include<stdio.h>

int sumFun(int x){

	static int sum = 0;
	sum = sum + x;

	if(x >1){
		sumFun(--x);
	}

	return sum;
}

void main(){

	int ret = sumFun(10);
	printf("%d\n",ret);
}
