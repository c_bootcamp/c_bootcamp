//**MY**palindromic string (with recursion)

#include<stdio.h>

int palStr(char arr[],int start,int end){

	if(start >= end){
		return 1;
	}
	
	if(arr[start] != arr[end])
		return -1;

	//pre-increment/decrement of variable(++var) or direct increment/decrement like (var+1) while passing is okk..but post increment/decrement(varr++) leads to a 'Segmentstion fault'
	palStr(arr,++start,--end);
}

void main(){

	int start = 0;
	char arr[]={'m','a','d','a','m'};
	int end = sizeof(arr)/sizeof(char);

	int ret = palStr(arr,start,end-1);

	if(ret == 1)
		printf("palindromic string!!\n");
	else
		printf("not a palindromic string\n");
}
