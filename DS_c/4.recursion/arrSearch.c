#include<stdio.h>

int searchEle(char arr[],int size,char search){

	if(size < 0)	
		return -1;
	if(arr[size] == search)
		return size;

	return searchEle(arr,size-1,search);
}

void main(){

	char arr[] = {'s','h','w','e','t','a'};

	char ch ;
	
	printf("enter the character to search :\n");
	scanf(" %c",&ch);
	
	int size = sizeof(arr)/sizeof(arr[0]);

	int ret = searchEle(arr,size-1,ch);	//actual index pathvtey mhanu size-1..direct size pathvl tr vr size-1 ne sgl handle krav lagel
	
	if(ret == -1)
		printf("%c not present in a given array !!\n",ch);
	else
		printf("%c present at %d index in the array !!\n" , ch,ret);
}
