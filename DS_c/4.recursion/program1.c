//print 10 to 1 using recursion

#include<stdio.h>

void recFun(int x){

	printf("%d\n",x--);

	if(x>0)
		recFun(x);
}

void main(){
	recFun(10);
}
