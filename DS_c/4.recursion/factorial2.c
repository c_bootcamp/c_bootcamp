//(non-tale recursion)factorial code using recursion --> without extra variable
//CONSTRAINS : num >= 0


#include<stdio.h>

int factorialNo(int num){

	if(num <= 1)
		return 1;

	return factorialNo(num-1) * num;
}

void main(){

	int num;

	printf("enter any number :\n");
	scanf("%d",&num);
	
	int fact = factorialNo(num);

	printf("Factorial of %d is %d \n",num,fact);
}
