/* prog : count zero [with recursion]
   recursion mhanje loop ch ch kam krt..mhanun loop chi grj nste recursion mdhe pn ek stopping condition aste */ 

#include<stdio.h>

int countZero(int num){

	static int count = 0;

	if(num == 0){

		count++;
		return count;
	}

	int rem = num % 10;
	num = num / 10; 

	if(rem == 0)
		count++;

	if(num != 0)
		countZero(num);

	return count;
}

void main(){

	int num;
	printf("enter number :\n");
	scanf("%d",&num);

	int count = countZero(num);

	printf("number of zeros in %d is %d \n",num,count);
}
