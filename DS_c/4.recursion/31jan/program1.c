/* prog : count zero's [count number of zeros without recursion]*/

#include<stdio.h>

int countZero(int num){

	int count = 0;

	if(num == 0){
	
		count = 1;
		return count;
	}

	while(num){

		int rem = num % 10;
		num = num / 10;

		if(rem == 0)
			count++;
	}
	
	return count;
}

void main(){

	int num;

	printf("enter any number :\n");
	scanf("%d",&num);

	int count = countZero(num);

	printf("Number of zero's in %d is %d \n",num,count);
}

