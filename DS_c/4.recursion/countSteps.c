//count steps : if num is even then divide by 2 and if odd then substract 1 and continue till 0...count those steps

#include<stdio.h>

int countSteps(int num,int count){

	if(num == 0)
		return count;

	if(num%2 == 0)
		return countSteps(num/2,count+1);
	if(num%2 != 0)
		return countSteps(num-1,count+1);

}

void main(){

	int num;

	printf("enter any number :\n");
	scanf("%d",&num);

	int count = 0;
	count = countSteps(num,count);

	printf("number of steps for %d are %d\n",num,count);

}
