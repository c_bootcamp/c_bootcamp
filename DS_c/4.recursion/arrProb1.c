//if more than 2 even numbers are present then return true otherwise false

#include<stdio.h>
#include<stdbool.h>

bool checkArr(int arr[],int size,int count){

	if(count > 2)
		return 1;
	if(size == 0)
		return 0;

	if(arr[size-1]%2 == 0)
		return checkArr(arr,size-1,count+1);
	if(arr[size-1]%2 != 0)
		return checkArr(arr,size-1,count);
}

void main(){

	int size;

	printf("enter array size :\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter array elements : \n");
	for(int i=0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printf("array is :\n");
	for(int i=0; i<size; i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
	
	int count = 0;
	bool ret = checkArr(arr,size,count);

	if(ret)
		printf("true : more than 2 even numbers in given array!!\n");
	else
		printf("false : there are no more than 2 even numbers in given array!!\n");

}
